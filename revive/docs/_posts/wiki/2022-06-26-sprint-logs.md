---
layout: post
title: "Sprint Tasks"
excerpt: "References for Tasks completed in Sprint"
categories: wiki
tags: [ wiki ]

date: 2022-06-26T08:08:50-04:00
---

# Sprint Logs
* Sprint 23 - October -16-23
  * Space Apps
    * https://2022.spaceappschallenge.org/locations/magdeburg/teams
    * https://2022.spaceappschallenge.org/challenges/2022-challenges/mars-habitat/teams/gaganyatri/project
  * GitHub advanced security
    * https://resources.github.com/security/tools/ghas-trial/
  * ALpine linux - 
    * apk update
    * apk add --no-cache bash git openssh 
  * git submodules - github actions 
    * https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token
    * https://github.com/actions/checkout/issues/14
  * MariaDB
    * https://mariadb.com/resources/blog/get-started-with-mariadb-using-docker-in-3-steps/
    * https://hub.docker.com/_/mariadb/
    * https://mariadb.com/kb/en/installing-and-using-mariadb-via-docker/
    * https://mariadb.org/
  * Mono repo -submodules
    *  git clone  https://github.com/slabstech/revive.git --recursive
    * https://stackoverflow.com/questions/10666085/how-do-i-get-git-clone-recursive-to-recreate-submodules-remotes-and-branche
    * https://stackoverflow.com/questions/46462610/monorepo-vs-github-submodules
    * https://support.tmatesoft.com/t/get-your-code-together-submodules-monorepo-or-the-third-way/2536
    * https://rebit.co/blog/setup-git-monorepo-for-javascript-typescript
    * https://www.jannikbuschke.de/blog/git-submodules/
* Sprint 21 -
  * Monorepo
    * Rush - https://medium.com/@sharadghimire5551/creating-monorepos-on-github-2e0e65d36a92
    * https://www.atlassian.com/git/tutorials/monorepos
    * https://github.com/korfuri/awesome-monorepo
    * https://engineering.fb.com/2014/01/07/core-data/scaling-mercurial-at-facebook/
    * https://medium.com/@adamhjk/monorepo-please-do-3657e08a4b70
    * https://gigamonkeys.com/mono-vs-multi/
    * https://danluu.com/monorepo/
    * https://github.com/thundergolfer/example-bazel-monorepo
    * https://github.com/acntech/bazel-monorepo-demo
    * https://github.com/lokshunhung/bazel-ts-monorepo
    * https://github.com/emurova/monorepo-bazel
  * Opensource
    * https://github.com/valhalla/valhalla
    * https://www.swissgeol.ch/en
    * https://opensourceindex.io/
* Sprint 17 - Sep 18-24
  * Go references
    * https://medium.com/kanoteknologi/better-way-to-read-and-write-json-file-in-golang-9d575b7254f2
  * Dev containers for codespace
    * https://docs.github.com/en/codespaces/setting-up-your-project-for-codespaces/introduction-to-dev-containers
    * https://docs.github.com/en/codespaces/setting-up-your-project-for-codespaces/introduction-to-dev-containers#using-a-predefined-dev-container-configuration
    * https://docs.github.com/en/codespaces/developing-in-codespaces/creating-a-codespace#creating-a-codespace
    * https://containers.dev/implementors/json_reference/
  * EmbeddedJava
    * https://kotlinlang.org/docs/mixing-java-kotlin-intellij.html
    * https://discuss.kotlinlang.org/t/embedding-kotlin-as-scripting-language-in-java-apps/2211
    * https://blog.jetbrains.com/kotlin/2018/04/embedding-kotlin-playground/
    * https://lightrun.com/answers/quarkusio-quarkus-using-embedded-with-kotlin-results-in-propertynotfoundexception-exception
    * https://napperley.medium.com/case-for-kotlin-in-embedded-development-d52298ed7dbb
    * https://www.baeldung.com/jpa-embedded-embeddable
    * https://www.callicoder.com/hibernate-spring-boot-jpa-embeddable-demo/
    * https://github.com/quarkusio/quarkus
    * https://mkyong.com/maven/maven-error-invalid-target-release-17/
    * https://docs.github.com/en/actions/automating-builds-and-tests/building-and-testing-java-with-maven
    * Draft pr - github cli- https://cli.github.com/manual/gh_pr_create 
    * Install node -https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04
    * CodeCov - https://docs.codecov.com/docs
    * JavaOCP - https://github.com/ubitricity/Java-OCA-OCPP
* Sprint 15 - Sep 4-10
  * CODEOWNERS -https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/customizing-your-repository/about-code-owners 
  * ReactJS GitHub action 
    * https://dev.to/dyarleniber/setting-up-a-ci-cd-workflow-on-github-actions-for-a-react-app-with-github-pages-and-codecov-4hnp
    * https://blog.logrocket.com/ci-cd-pipelines-react-github-actions-heroku/
  * Setup Go GitHub Action
    * https://blog.kowalczyk.info/article/8dd9c2c0413047c589a321b1ccba7129/using-github-actions-with-go.html
    * https://www.alexedwards.net/blog/ci-with-go-and-github-actions
    * https://github.com/actions/setup-go
  * Build Go With Gradle
    * https://levelup.gitconnected.com/automate-your-go-applications-with-gradle-docker-3c4aa4ddd9bb
  * Rpi Setup - 
    * GitHub CLI - https://lindevs.com/install-github-cli-on-raspberry-pi/
    * java - https://linuxize.com/post/install-java-on-raspberry-pi/
    * sdkman - https://sdkman.io/install
    * gradle - https://gradle.org/install/
  * Android Release
    * https://medium.com/scalereal/devops-ify-android-libraries-with-github-actions-and-package-registry-5e7f69a83622
    * https://github.com/PatilShreyas/AndroidGPR
    * https://proandroiddev.com/publishing-android-libraries-to-the-github-package-registry-part-1-7997be54ea5a
    * https://medium.com/@stpatrck/publish-an-android-library-to-github-packages-8dfff3ececcb
    * https://proandroiddev.com/how-to-securely-build-and-sign-your-android-app-with-github-actions-ad5323452ce

* Sprint 14 - Aug 29- Sep 4
  * Go
    * GoLand - https://www.jetbrains.com/help/go/getting-started.html
    * Create Project - https://www.jetbrains.com/help/idea/create-a-project-with-go-modules-integration.html
    * Go plugin - https://www.jetbrains.com/help/idea/go-plugin.html
  * Lakshmi Project
    * Multiplatform - Kotlin/JS - https://kotlinlang.org/docs/multiplatform.html 
  * GitHub CLI
    * https://cli.github.com/manual/gh_pr_create
  * GitHub CLI and self-hosted runners
    * https://github.com/cli/cli#installation
    * https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners
    * https://docs.github.com/en/actions/hosting-your-own-runners/autoscaling-with-self-hosted-runners#controlling-runner-software-updates-on-self-hosted-runners
    * https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners#supported-architectures-and-operating-systems-for-self-hosted-runners
    * https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners#communication-requirements
  * Golang best practices
   * https://go.dev/talks/2013/bestpractices.slide#1
   * https://go.dev/doc/effective_go
   * https://go.dev/doc/go_spec.html
   * https://golangdocs.com/golang-best-practices
   * https://golang.org/doc/effective_go
   * https://www.bacancytechnology.com/blog/go-best-practices
  * Solve Git submodules access issue in docker image creation
   * https://www.webfactory.de/blog/use-ssh-key-for-private-repositories-in-github-actions
   * https://github.com/marketplace/actions/webfactory-ssh-agent
  * Android Build
   * https://developer.android.com/studio/build/building-cmdline
* Sprint 13 - Aug 21-28
  * Android App
    * https://ncorti.com/blog/howto-github-actions-building-android 
    * https://medium.com/@owumifestus/configuring-github-actions-in-a-multi-directory-repository-structure-c4d2b04e6312
    * https://dev.to/shofol/run-your-github-actions-jobs-from-a-specific-directory-1i9e 
    * https://medium.com/scalereal/devops-ify-android-libraries-with-github-actions-and-package-registry-5e7f69a83622
    * https://docs.github.com/en/packages/managing-github-packages-using-github-actions-workflows/publishing-and-installing-a-package-with-github-actions
    * https://github.com/marketplace/actions/upload-android-release-to-play-store 
    * https://www.geeksforgeeks.org/automated-release-for-android-using-github-actions/
  * Data Mapping
    * https://stackoverflow.com/questions/3013655/creating-hashmap-map-from-xml-resources

* Sprint 11 - Aug 7- 13
  * Mono Repo
    * Git Submodules
      * https://www.atlassian.com/git/tutorials/git-submodule
    * https://www.atlassian.com/git/tutorials/monorepos
    * https://www.jannikbuschke.de/blog/git-submodules/
    * https://git-scm.com/book/en/v2/Git-Tools-Submodules
    * https://support.tmatesoft.com/t/get-your-code-together-submodules-monorepo-or-the-third-way/2536
  * Book Release
    * https://github.com/joeyespo/grip
    * https://github.com/joeyespo/grip#generate-html-documentation-from-a-collection-of-linked-readme-files
    * https://gist.github.com/justincbagley/ec0a6334cc86e854715e459349ab1446
    * https://wkhtmltopdf.org/downloads.html
    * https://github.com/puzzle/gitlab-ci-cd-training/blob/main/Dockerfile
  * Kubernetes
    * https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/
    * https://kubernetes.io/docs/tutorials/hello-minikube/
    * https://minikube.sigs.k8s.io/docs/start/
    * https://ubuntu.com/tutorials/how-to-kubernetes-cluster-on-raspberry-pi#1-overview
    * https://thepihut.com/products/cluster-case-for-raspberry-pi
    * https://www.amazon.de/-/en/GeeekPi-Raspberry-Cluster-Stackable-Jetson/dp/B08FHJ5TR2/ref=sr_1_4?keywords=pi+cluster+geh%C3%A4use&qid=1659881071&sprefix=pi+cluster%2Caps%2C92&sr=8-4
    * https://developer.nvidia.com/buy-jetson?product=jetson_nano&location=DE

* Sprint 10 - Jul 30-Aug 6
* Kotlin testcontainers
  * https://www.wwt.com/article/using-testcontainers-for-unit-tests-with-spring-and-kotlin
  * https://kotest.io/docs/extensions/test_containers.html
* test retrofit authinterceptor
  * https://medium.com/@michaelbukachi/testing-retrofit-interceptors-4eaae636077b
  * https://stackoverflow.com/questions/33788568/good-way-to-unit-test-retrofit-interface-declaration
* Exception Handling - https://betterprogramming.pub/do-you-even-try-functional-error-handling-in-kotlin-ad562b3b394f


* Sprint 8- July 11-15

* Kotlin with drowizard
  * https://levelup.gitconnected.com/create-an-app-with-dropwizard-maven-and-kotlin-24a6277b946c
  * https://github.com/jecklgamis/dropwizard-kotlin-example
  * https://nickb.dev/blog/integrating-kotlin-with-a-java-dropwizard-app/
  * https://medium.com/@ActivoutDavid/dropwizard-kotlin-project-kronslott-e2aa51b277b8
  * http://mannanlive.com/2019/03/03/building-a-kotlin-dropwizard-maven-rest-api-from-scratch/
* Container - https://www.wwt.com/article/using-testcontainers-for-unit-tests-with-spring-and-kotlin
  - https://github.com/wwt/spring-boot-kotlin-testcontainers
  * https://github.com/jdemattos/kotlin-dropwizard-pet-license-service
* kotlin - https://github.com/Baeldung/kotlin-tutorials 
* Working with date : https://www.baeldung.com/kotlin/dates

* mocking retrofit library
  * https://riggaroo.dev/retrofit-2-mocking-http-responses/
  * https://sachinkmr375.medium.com/unit-test-retrofit-api-calls-with-mockwebserver-bbb9f66a78a6


* Sprint 6
# Retrofit HTTP library
 * https://github.com/eugenp/tutorials/tree/master/libraries-http
 * https://www.baeldung.com/retrofit

# okta Auth
 * https://developer.okta.com/blog/2017/10/27/secure-spa-spring-boot-oauth#get-your-oauth-info-ready
 * https://developer.okta.com/blog/2018/03/21/dropwizard-oauth
 * https://github.com/na703/DropwizardRethinkDB/blob/master/pom.xml


* Sprint 5 

# JetBrains - IntelliJ Plugin
 * Create Repo from Template - https://github.com/JetBrains/intellij-platform-plugin-template
 * Sign the Plugin - https://plugins.jetbrains.com/docs/intellij/plugin-signing.html
 * Upload to Marketplace - 
   * https://plugins.jetbrains.com/docs/intellij/update-plugins-format.html#optional-updatepluginxml-elements
   * https://account.jetbrains.com/licenses
   * 
 * tutorial video - https://www.youtube.com/watch?v=vAlor5-hC0Q


* Sprint 4

# version 8
* Gradle Dropwizard
  * https://karollotkowski.wordpress.com/2015/10/13/run-dropwizard-with-gradle/
  * https://github.com/dropwizard/dropwizard/tree/master/dropwizard-example

  * To Test - HelloWorldApplication.java
    * http://localhost:8080/hello-world?name=Successful+Dropwizard+User
    * To Get metric
      * http://localhost:8081/metrics
  * Dropwizard Manual
    * https://www.dropwizard.io/en/latest/manual/configuration.html

  * ./gradlew build --refresh-dependencies
  * https://jdbc.postgresql.org/documentation/head/connect.html  _ set property- Intergration test

# version 7
* Composite Github Actions
  * https://docs.github.com/en/actions/using-workflows/about-workflows

  * https://docs.github.com/en/actions/using-workflows/workflow-syntax-for-github-actions

  * https://docs.github.com/en/actions/using-workflows/reusing-workflows

  * https://github.blog/2022-02-10-using-reusable-workflows-github-actions/


* Example -
  * https://wallis.dev/blog/composite-github-actions

  * https://betterprogramming.pub/streamline-your-github-actions-with-composite-actions-a8ebc6d28f6b
  
* Deploy to Azure
  * https://docs.microsoft.com/en-us/azure/app-service/deploy-github-actions?tabs=applevel
  * node.js - https://docs.microsoft.com/en-us/azure/app-service/quickstart-nodejs
  * java - https://docs.microsoft.com/en-us/azure/app-service/quickstart-python
  * https://github.com/Azure/actions-workflow-samples
  * https://github.com/Azure/actions

* Buildkit  -
* https://medium.com/titansoft-engineering/docker-build-cache-sharing-on-multi-hosts-with-buildkit-and-buildx-eb8f7005918e
# version 6

* Publish multiple dockers from mono repo
  * https://github.com/marketplace/actions/publish-docker
* Dropwizard Demo
  * maven - https://howtodoinjava.com/dropwizard/tutorial-and-hello-world-example/
  * https://www.dropwizard.io/en/latest/getting-started.html
  * https://levelup.gitconnected.com/create-an-app-with-dropwizard-maven-and-kotlin-24a6277b946c
  * gradle
    * https://automationrhapsody.com/build-dropwizard-project-gradle/
    * https://github.com/ireardon/dropwizard-example-gradle
    * https://www.anuragkapur.com/blog/programming/java/2017/06/21/gradle-dropwizard-uber-jar.html

* docker compoase AWS - https://aws.amazon.com/blogs/containers/deploy-applications-on-amazon-ecs-using-docker-compose/

* https://aws.amazon.com/blogs/containers/automated-software-delivery-using-docker-compose-and-amazon-ecs/

* https://docs.docker.com/cloud/ecs-integration/

* docker- local to AWS
 - https://www.docker.com/blog/docker-compose-from-local-to-amazon-ecs/


* Sprint 3

* Install Docker CLI 

curl -L https://raw.githubusercontent.com/docker/compose-cli/main/scripts/install/install_linux.sh | sh

* AWS DEploy
* https://docs.github.com/en/actions/deployment/deploying-to-your-cloud-provider/deploying-to-amazon-elastic-container-service
* https://github.com/actions/starter-workflows/blob/main/deployments/aws.yml
* https://github.com/aws-actions/amazon-ecs-render-task-definition
* https://github.com/aws-actions/amazon-ecs-deploy-task-definition
* https://aws.plainenglish.io/build-a-docker-image-and-publish-it-to-aws-ecr-using-github-actions-f20accd774c3 

AWS IAM - https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html

https://github.com/aws-actions/configure-aws-credentials


# Version 5 

* Unit Test
    * https://www.baeldung.com/jpa-default-column-values

SpringBoot TDD -
https://tanzu.vmware.com/content/springone-platform-2017/test-driven-development-with-spring-boot-sannidhi-jalukar-madhura-bhave



* Github Actions Runner
    * https://docs.github.com/en/actions/using-workflows/events-that-trigger-workflows


* Push multiple dockerfiles to docker Hub
https://github.com/docker/build-push-action/issues/561

    * https://stackoverflow.com/questions/62750603/github-actions-trigger-another-action-after-one-action-is-completed


* Add your Docker ID as a secret to GitHub. Navigate to your GitHub repository and click Settings > Secrets > New secret.
* Create a new secret with the name DOCKER_HUB_USERNAME and your Docker ID as value.
* Create a new Personal Access Token (PAT). To create a new token, go to Docker Hub Settings and then click New Access Token.
* Enter value as project-name ****

* setup secrets : https://docs.docker.com/ci-cd/github-actions/
* Docker CI/CD Best Practice : https://www.docker.com/blog/best-practices-for-using-docker-hub-for-ci-cd/
* https://tutorials.releaseworksacademy.com/learn/building-your-first-docker-image-with-jenkins-2-guide-for-developers
* https://github.com/marketplace/actions/build-and-push-docker-images
* https://docs.docker.com/ci-cd/github-actions/
* https://www.prestonlamb.com/blog/creating-a-docker-image-with-github-actions

* READ These
    * https://docs.github.com/en/actions/security-guides/automatic-token-authentication
    * https://github.com/docker/build-push-action/tree/master/docs/advanced
    * https://docs.github.com/en/packages/working-with-a-github-packages-registry/working-with-the-container-registry
    *





# Version 4

|No. | Step | Status | Date |
|---|---|---|---|
|1.| JRE Size Reduction/Custom JRE | Done  | 27-May-22 |
|2.| Split WebApp : Front End + Back End | WIP  | 27-May-22 |


* VSCode Setup + codespace

* Install OpenJDK17 -  https://adoptium.net/temurin/releases/?version=17

* Make builds for
    * 0.1 - Core Java + PostgreSQL
    * 0.2 - SpringBoot + Hibernate JPA
    * 0.3 - UT with Thymeleaf + v0.2
    * 0.4 - ReactJS frontend integration


* Create custom jre17 docker - https://levelup.gitconnected.com/java-developing-smaller-docker-images-with-jdeps-and-jlink-d4278718c550
    * https://dzone.com/articles/dockerizing-with-a-custom-jre
    * https://blog.adoptium.net/2021/08/using-jlink-in-dockerfiles/
    * https://github.com/fedeoliv/java-custom-jre
    * https://blog.adoptium.net/2021/10/jlink-to-produce-own-runtime/
    *  https://shekhargulati.com/2019/01/13/running-tests-and-building-react-applications-with-gradle-build-tool/
   * Jlink - fix objcopy : https://medium.com/@david.delabassee/jlink-stripping-out-native-and-java-debug-information-507e7b587dd7

* ReactJS + SpringBoot Split
    * https://www.callicoder.com/spring-boot-mysql-react-docker-compose-example/
    * material UI - https://medium.com/geekculture/a-reactjs-web-application-with-a-spring-boot-backend-and-containerizing-it-using-docker-3eeaed8cb45a
    * multi-stage docker - https://blog.obeosoft.com/multi-stage-docker-build-for-react-and-spring

* nodejs docker
    * https://github.com/nodejs/docker-node/blob/main/README.md#how-to-use-this-image
    * https://hub.docker.com/_/node
    * http://nginx.org/
    * https://tiangolo.medium.com/react-in-docker-with-nginx-built-with-multi-stage-docker-builds-including-testing-8cc49d6ec305
    * https://medium.com/bb-tutorials-and-thoughts/how-to-serve-react-application-with-nginx-and-docker-9c51ac2c50ba
    * https://dev.to/bahachammakhi/dockerizing-a-react-app-with-nginx-using-multi-stage-builds-1nfm


* Spring Rest API 
    * https://hevodata.com/learn/spring-boot-rest-api/,



# Version 3

|No. | Step | Status | Date |
|---|---|---|---|
|1.| UI | WIP  | 25-May-22 |
|2.| Github actions | Done | 23-May-22|
|3.| Readme status links | Done | 23-May-22 |
|4.| Integrate ReactJS Front-end | Done | 24-May-22 |
|5.| Integrate Oauth | -- | --|
|6.| docker compose for AWS |-- | -- |
|7.| Gradle pack  Reach + SpringBoot | WIP | 25-5-22 |


Source for steps
* Github Actions - 
  * https://tomgregory.com/build-gradle-projects-with-github-actions/

* Micro Service -  
  * https://dzone.com/articles/build-and-package-a-microservices-architecture-wit

* ReactJS -
  * https://spring.io/guides/tutorials/react-and-spring-data-rest/
  * https://github.com/eugenp/tutorials/tree/master/spring-boot-modules/spring-boot-react

* MongoDB - 
  * Spring - https://spring.io/guides/gs/accessing-mongodb-data-rest/

* Oauth - Oktta -
  * https://developer.okta.com/docs/guides/sign-into-web-app-redirect/spring-boot/main/#redirect-to-the-sign-in-page
  * https://github.com/okta/samples-java-spring/tree/master/okta-hosted-login
  * oauth .- https://www.baeldung.com/spring-security-oauth

* Spring Live reload - 
  * https://www.codejava.net/frameworks/spring-boot/spring-boot-auto-reload-changes-using-livereload-and-devtools

Init DB ;
* custom sequence
  * https://thorben-janssen.com/hibernate-tips-use-custom-sequence/
* https://www.baeldung.com/spring-boot-data-sql-and-schema-sql
* https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto.data-initialization.using-basic-sql-scripts
* https://www.baeldung.com/hibernate-identifiers
* https://github.com/eugenp/tutorials/tree/master/persistence-modules/spring-boot-persistence

* Gradle split
* https://www.petrikainulainen.net/programming/gradle/getting-started-with-gradle-creating-a-multi-project-build/


* Integrating Gradle + Spring Boot + React 
	* https://github.com/node-gradle/gradle-node-plugin/blob/master/docs/usage.md
	* https://github.com/rewolf/blog-spring-and-react-1-setup/tree/master/src/main/webapp/javascript
	* https://andrew-flower.com/blog/Spring-Boot-and-React-1



Sprint 2

# Version 2

|No. | Step | Status | Date |
|--|--|--|--|
|1.| Connect to Postgres JDBC| Done | 18-May-22 |
|2.| Connect Postgres with  Hibernate | Done | 18-May-22| 
|3.| Update to SpringMVC | Done | 19-May-22|
|4.| Hibernate Map Object | Done | 20-May-22|
|5.| Simple UI - Display All Table Info | Done | 24-May-22 |
|7 | Exception Handling |Done |21-5-22| 
|8 |Setup Docker - SpringBoot + PostgreSQL|Done |22-MAy-25 | 


Source for steps

* sprint boot bootstrap
	* https://github.com/eugenp/tutorials/tree/master/spring-boot-modules/spring-boot-bootstrap
	* https://www.baeldung.com/spring-boot-start
	* https://spring.io/guides/gs/convert-jar-to-war/
	* SpringBoot Gradle - https://docs.spring.io/spring-boot/docs/2.6.7/gradle-plugin/reference/htmlsingle/
	* baeldung - https://github.com/eugenp/tutorials/tree/master/spring-boot-modules/

* Test Case Setup
	* https://github.com/eugenp/tutorials/blob/master/spring-boot-modules/spring-boot-bootstrap/src/test/java/com/baeldung/SpringBootBootstrapLiveTest.java	

* Exception handling

	* https://github.com/eugenp/tutorials/blob/master/spring-boot-modules/spring-boot-bootstrap/src/main/java/com/baeldung/web/exception/BookIdMismatchException.java
	https://www.baeldung.com/spring-5-junit-config



Sprint 1

* Junit - 
	* https://www.baeldung.com/junit-5-gradle
* postgres

	* ubuntu - https://www.tecmint.com/install-postgresql-and-pgadmin-in-ubuntu/

	* create database avti;
	* grant all privileges on database avti to postgres;
	* https://www.postgresqltutorial.com/postgresql-jdbc/connecting-to-postgresql-database/
	* https://www.postgresqltutorial.com/postgresql-getting-started/install-postgresql/
	* https://www.postgresqltutorial.com/postgresql-getting-started/connect-to-postgresql-database/
	* https://thorben-janssen.com/hibernate-postgresql-5-things-need-know/
	* https://stackabuse.com/implementing-hibernate-with-spring-boot-and-postgresql/
	* ubuntu - https://www.pgadmin.org/download/pgadmin-4-apt/
 create database avti_db;
	* grant all privileges on database avti_db to avti_db;
	* Access PSQL
		* psql -d postgres -U postgres 
	* https://www.enterprisedb.com/postgres-tutorials/connecting-postgresql-using-psql-and-pgadmin
* Docker
	* https://www.baeldung.com/spring-boot-postgresql-docker
	* docker reduce . https://medium.com/@ievgen.degtiarenko/reduce-size-of-docker-image-with-spring-boot-application-2b3632263350
	* recuing jre sizw - https://www.baeldung.com/jlink
	* https://hub.docker.com/r/bellsoft/liberica-openjdk-alpine

* Spring JPA
	* Hibernate Mapping - https://thorben-janssen.com/ultimate-guide-association-mappings-jpa-hibernate/
	* https://thorben-janssen.com/entity-mappings-introduction-jpa-fetchtypes/
	* https://www.baeldung.com/hibernate-query-to-custom-class

* Spring MVC
	* https://www.learn-it-with-examples.com/development/java/thymeleaf/display-database-table-java-thymeleaf.html
	* https://www.codementor.io/@olebueziobinnadavid/setting-up-and-displaying-a-list-of-objects-in-a-table-thymeleaf-1cifoviz5e
	* https://bushansirgur.in/spring-boot-thymeleaf-display-list-of-records-from-database/
	https://spring.io/guides/gs/serving-web-content/
	* https://frontbackend.com/thymeleaf/spring-boot-bootstrap-thymeleaf-datatable
	* https://github.com/ro6ley/java-hibernate-example
	* https://spring.io/guides/gs/consuming-rest/
	* https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mvc.html
	* Spring MVC - https://www.baeldung.com/spring-mvc-tutorial
	

* Docker
* https://github.com/docker-library/postgres/blob/4e56664f1797ba4cc0f5917b6d794792a5571b45/14/alpine/Dockerfile
* https://github.com/docker-library/docs/blob/master/postgres/README.md
* https://hub.docker.com/_/postgres
* https://snapshooter.com/learn/postgresql/deploy-postgres-with-docker
* https://hub.docker.com/r/cafapi/java-postgres
* https://www.baeldung.com/spring-boot-postgresql-docker
* https://hub.docker.com/_/openjdk






### Day 1

# Version 1

|No. | Step | Status | Date |
|--|--|--|--|
|1.| Run main class from Server.java eclipse control panel| Done | 18-May-22 |
|2.| Generate artifacts for deployment | Done| 18-May-22|
|3.| Create Test Suite|Done|20-May-22|
|4.| Run from apache-tomcat/spring boot | Done |21-5-22|
|5.| Create github action to build | Done | 18-May-22|


Source for steps
* https://www.jetbrains.com/help/space/publish-artifacts-from-a-gradle-project.html
* https://www.vogella.com/tutorials/EclipseGradle/article.html
* createing fat jar or uber-jar :  https://www.baeldung.com/gradle-fat-jar
* shadowJar - https://imperceptiblethoughts.com/shadow/publishing/#publishing-with-maven-publish-plugin
* Conversion Steps
	* Add build.gradle file in root project directory
	* Add configurations
	* Close the project in Eclipse and Re.import as Gradle Project
	* In Gradle Tasks menu bar, run "gradle init"
* Installing gradle in linux - Ubuntu : https://linuxize.com/post/how-to-install-gradle-on-ubuntu-18-04/ :: install version 6.8 
* In terminal run "gradle build"
* java -jar build/libs/reviveJar-1.0-SNAPSHOT.jar
* Setup Gradle with github action :  https://tomgregory.com/build-gradle-projects-with-github-actions/
