# Revive

![build](https://github.com/slabstech/revive/actions/workflows/build.yml/badge.svg)
[![Wiki](https://github.com/slabstech/revive/actions/workflows/jekyll-gh-pages.yml/badge.svg)](https://github.com/slabstech/revive/actions/workflows/jekyll-gh-pages.yml)
![docker](https://github.com/slabstech/revive/actions/workflows/docker_images.yml/badge.svg)
![SpringBoot](https://github.com/slabstech/revive/actions/workflows/spring_boot.yml/badge.svg) ![Dropwizard](https://github.com/slabstech/revive/actions/workflows/dropwizard.yml/badge.svg)
![ReactJS](https://github.com/slabstech/revive/actions/workflows/reactjs.yml/badge.svg)


* Open source, Swarm Robots as a Platform


* Principles
  * 3-Step Rule
  * Infrastructure as Code
  * Write Once, Run Anywhere
  * Platform independent
  * 5 sec Rule
  * Runnable on RPi Pico 

* 3-Step Rule 
  * Setup
    * Checkout code
      ````
      git clone --depth 1 --branch main https://github.com/slabstech/revive.git --recursive 
      ````
  * Build
    * Build monorepo
      ````
      gradle buildAll 
      ````
  * Run
    * Run Docker Compose
      ````
      docker-compose up -d 
      ````


* Monorepo


| Name                                                                                                    | Repo                                                                                                                   | Integrated |
|---------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------|------------|
| [slabstech.com/revive](https://github.com/slabstech/revive)                                             | [https://github.com/slabstech/revive](https://github.com/slabstech/revive)                                             | Yes        |
| [gaganyatri.com](https://github.com/slabstech/gaganyatri.com)                                           | [https://github.com/slabstech/gaganyatri.com](https://github.com/slabstech/gaganyatri.com)                             | No         |
| [slabstech.com](https://github.com/slabstech/slabstech.github.io)                                       | [https://github.com/slabstech/slabstech.github.io](https://github.com/slabstech/slabstech.github.io)                   | No         |
| [mangala.earth](https://github.com/slabstech/bhoomi)                                                    | [https://github.com/slabstech/bhoomi](https://github.com/slabstech/bhoomi)                                             | No         |
| [slabstech.com/connectthedots.com](https://github.com/slabstech/connectthedots.com)                     | [https://github.com/slabstech/connectthedots.com](https://github.com/slabstech/connectthedots.com)                     | No         |
| [slabstech.com/ammanaaduge.com](https://github.com/slabstech/ammanaaduge.com)                           | [https://github.com/slabstech/ammanaaduge.com](https://github.com/slabstech/ammanaaduge.com)                           | No         |
| [slabstech.com/samruddhi.com](https://github.com/slabstech/samrudhi)                                    | [https://github.com/slabstech/samrudhi](https://github.com/slabstech/samrudhi)                                         | No         |
| [slabstech.com/keeper](https://github.com/slabstech/keeper)                                             | [https://github.com/slabstech/keeper](https://github.com/slabstech/keeper)                                             | No         |
| [slabstech.com/alemaari.in](https://github.com/slabstech/alemaari.in)                                                 | [https://github.com/slabstech/alemaari.in](https://github.com/slabstech/alemaari.in)                                   | No         |
| [action-deploy-container-to-registry](https://github.com/slabstech/action-deploy-container-to-registry) | [https://github.com/slabstech/action-deploy-container-to-registry](https://github.com/slabstech/action-deploy-container-to-registry) | No         |
| [action-cuda-compiler-python](https://github.com/slabstech/action-cuda-compiler-python)                 | [https://github.com/slabstech/action-cuda-compiler-python](https://github.com/slabstech/action-cuda-compiler-python)   | No         |
| [action-cuda-compiler](https://github.com/slabstech/action-cuda-compiler)                               | [https://github.com/slabstech/action-cuda-compiler](https://github.com/slabstech/action-cuda-compiler)                 | No         |
| [action-create-ebook](https://github.com/slabstech/action-create-ebook)                                 | [https://github.com/slabstech/action-create-ebook](https://github.com/slabstech/action-create-ebook)                   | No         |
| [slabstech.com/darktales.com](https://github.com/slabstech/darktales.com)                                             | [https://github.com/slabstech/darktales.com](https://github.com/slabstech/darktales.com)                             | No         |
| [slabstech.com/aachocolates.in](https://github.com/slabstech/aachocolates.in)                                         | [https://github.com/slabstech/aachocolates.in](https://github.com/slabstech/aachocolates.in)                             | No         |
| [slabstech.com/thehdtour.com](https://github.com/slabstech/thehdtour.com)                                             | [https://github.com/slabstech/thehdtour.com](https://github.com/slabstech/thehdtour.com)                             | No         |
| [slabstech.com/ant2Maven](https://github.com/slabstech/ant2Maven)                                                     | [https://github.com/slabstech/ant2Maven](https://github.com/slabstech/ant2Maven)                                           | No         |



#### Reference
* Documents and build steps [maintained at Wiki - ](https://slabstech.com/revive/)[https://slabstech.com/revive/](https://slabstech.com/revive/)

* [Bootstrap a Saas Product](https://gaganyatri.com/build/building-hpc-saas-startup-from-browser//)

* [Why mono-repo](https://danluu.com/monorepo/)


### Tech Stack
* v.0.0.1 
  * Java + SpringBoot + ReactJS + PostgreSQL + Docker + Maven/Gradle + Kubernetes + REST API

#### Sponsors
* Built with [JetBrains OpenSource License](https://jb.gg/OpenSourceSupport) 
* S Labs : Perpetual Machines

#### Contributors

Revive is awaiting Contributors for creating more templates

* Designed with ideas from "Software Architecture Elevator" from Gregor Hohpe 
