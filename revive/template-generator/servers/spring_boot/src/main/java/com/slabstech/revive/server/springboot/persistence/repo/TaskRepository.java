package com.slabstech.revive.server.springboot.persistence.repo;


import com.slabstech.revive.server.springboot.persistence.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
