package com.slabstech.revive.server.springboot.persistence.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "task")
@EntityListeners(AuditingEntityListener.class)
public class Task {


  @Id
  @GeneratedValue
  @Column(name = "id")
	private long id;

	@NotBlank(message = "Task description is required")
	@Size(min = 4, max = 200)
	@Column(name = "description")
	String description;

	@Column(name = "priority")
	private long priority = 1;


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPriority() {
		return priority;
	}

	public void setPriority(long priority) {
		this.priority = priority;
	}

}
