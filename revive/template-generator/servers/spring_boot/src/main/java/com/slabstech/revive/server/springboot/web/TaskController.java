package com.slabstech.revive.server.springboot.web;


import com.slabstech.revive.server.springboot.persistence.model.Task;
import com.slabstech.revive.server.springboot.persistence.repo.TaskRepository;
import com.slabstech.revive.server.springboot.web.exception.TaskNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private Validator validator;

    /**
     * Get all Tasks list.
     *
     * @return the list
     */
    @GetMapping				   // GET Method for reading operation
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }


    /**
     * Update task response entity.
     *
     * @param taskId the task id
     * @param taskDetails the task details
     * @return the response entity
     * @throws Exception
     */
    @PutMapping("/{id}")    // PUT Method for Update operation
    public ResponseEntity<Task> updateTask(
            @PathVariable(value = "id") Long taskId, @Valid @RequestBody Task taskDetails)
            throws Exception {

        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(("Task " + taskId + " not found")));


        // Set the values for task here to update

        task.setDescription(taskDetails.getDescription());
        task.setPriority(taskDetails.getPriority());

        Set<ConstraintViolation<Task>> violations = validator.validate(task);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<Task> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }

            throw new ConstraintViolationException("Error occurred: " + sb.toString(), violations);
        }


        final Task updatedTask = taskRepository.save(task);
        return ResponseEntity.ok(updatedTask);
    }



    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
