package com.slabstech.revive.server.dropwizard.api

import com.fasterxml.jackson.annotation.JsonProperty
import kotlin.properties.Delegates

class Saying {
    @get:JsonProperty
    private var id by Delegates.notNull<Long>()


    @get:JsonProperty
    lateinit var content: String

    constructor() {
        // Jackson deserialization
    }

    constructor(id: Long, content: String) {
        this.id = id
        this.content = content
    }
}