# Installing gh on Linux and BSD

Packages downloaded from https://github.com/slabstech/revive/releases
are considered official binaries. We focus on popular Linux distros and
the following CPU architectures: `amd64`, `arm64`, `armhf`.

## Official sources

### Ubuntu Linux, Raspberry Pi OS (apt)

Install:

```bash
curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | sudo dd of=/usr/share/keyrings/githubcli-archive-keyring.gpg \
&& sudo chmod go+r /usr/share/keyrings/githubcli-archive-keyring.gpg \
&& echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | sudo tee /etc/apt/sources.list.d/github-cli.list > /dev/null \
&& sudo apt update \
&& sudo apt install gh -y
```

Upgrade:

```bash
sudo apt update
sudo apt install todo
```

## Manual installation

* [Download release binaries][releases page] that match your platform; or
* [Build from source](./source.md).


### Android

Android 7+ users can install via [Termux](https://wiki.termux.com/wiki/Main_Page):

```bash
pkg install gh
```

### Alpine Linux

Alpine Linux users can install from the [stable releases' community package repository](https://pkgs.alpinelinux.org/packages?name=github-cli&branch=v3.15).

```bash
apk add todo-cli
```

Users wanting the latest version of the CLI without waiting to be backported into the stable release they're using should use the edge release's
community repo through this method below, without mixing packages from stable and unstable repos.[^1]

```bash
echo "@community http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
apk add github-cli@community
```

#### TODO 
[releases page]: https://github.com/cli/cli/releases/latest
[arch linux repo]: https://www.archlinux.org/packages/community/x86_64/github-cli
[arch linux aur]: https://aur.archlinux.org/packages/github-cli-git
[^1]: https://wiki.alpinelinux.org/wiki/Package_management#Repository_pinning
