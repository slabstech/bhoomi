package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
)

type Task struct {
	Name   string
	Active bool
}

type Todo struct {
	Name   string
	Tasks  []Task
	Active bool
}

type exitCode int

const (
	exitOK     exitCode = 0
	exitError  exitCode = 1
	exitCancel exitCode = 2
	exitAuth   exitCode = 4
)

func main() {
	code := mainRun()
	os.Exit(int(code))
}

func mainRun() exitCode {

	if os.Args[1] == "list" && len(os.Args) == 2 {
		readData()
		return exitOK
	}

	if len(os.Args) == 1 {
		fmt.Println("Not enough arguments")
		return exitError
	}
	processArgs(os.Args)

	return exitOK
}

func writeData(todo Todo) {

	file, err := json.MarshalIndent(todo, "", " ")
	if err != nil {
		fmt.Println(err)
	}

	err = os.WriteFile("todo.json", file, 0600)
	if err != nil {
		log.Fatal(err)
	}

}

func editData(todo Todo) {
	file, err := json.MarshalIndent(todo, "", " ")
	if err != nil {
		fmt.Println(err)
	}

	err = os.WriteFile("todo.json", file, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func deleteData(todo Todo) {
	file, err := json.MarshalIndent(todo, "", " ")
	if err != nil {
		fmt.Println(err)
	}

	err = os.WriteFile("todo.json", file, 0600)
	if err != nil {
		log.Fatal(err)
	}
}
func processArgs(args []string) {

	cmdName := args[1]
	value := args[2]

	if cmdName == "create" || cmdName == "-c" {
		todolist := Todo{}
		todolist.Name = value
		todolist.Active = true
		writeData(todolist)
	}
	if cmdName == "delete" || cmdName == "-d" {
		todolist := Todo{}
		todolist.Name = value
		todolist.Active = false
		deleteData(todolist)
	}
	if cmdName == "edit" || cmdName == "-e" {
		todolist := Todo{}
		todolist.Name = value
		editData(todolist)
	}

}

func readData() {

	file, err := os.ReadFile("todo.json")
	if err != nil {
		log.Fatal(err)
	}

	todo := Todo{}
	err = json.Unmarshal(file, &todo)
	if err != nil {
		log.Fatal(err)
	}

	v := reflect.ValueOf(todo)
	//fmt.Printf(v.String())

	for i := 0; i < v.NumField(); i++ {
		//field := v.Field(i)
		//fmt.Printf(field.String())
		
		fmt.Println("Field:", v.Field, v.FieldByIndex([]int{i}))
	}

	

}
