# TODO App

### Challenge 
Your task is to design and implement a simple TODO Command Line Interface (CLI).

A TODO has a name and description. Each TODO can have a list of TASKS associated with the TODO. Each TASK has only a name.

The CLI should provide mechanisms to create, read, update and delete a TODO.

Furthermore, it should provide mechanisms to add and remove TASKS from the TODO as well as to update individual TASKS.

How you store/persist the TODOS and TASKS is up to you.

### Build
* `make` - builds the binary
* `make test` - runs the tests
* `sudo make install` - installs the binary to `/usr/local/bin/todo`
* `make clean` - cleans up the build artifacts


### Usage
```
* Create a TODO
  * todo create todo_name / todo -c todo_name
* Edit a TODO
  * todo edit todo_name / todo -e todo_name
* Delete a TODO
  * todo delete todo_name / todo -d todo_name
* List all TODOs
  * todo list / todo -l
* Add a TASK to a TODO
  * todo add todo_name task_name / todo -a todo_name task_name
* Remove a TASK from a TODO
  * todo remove todo_name task_name / todo -r todo_name task_name
* Update a TASK
  * todo update todo_name task_name / todo -u todo_name task_name
* List all TASKs of a TODO
  * todo list todo_name / todo -l todo_name
```


### Implementation
* DataStructure
  * Tree based on git implementation
  * Directories as Tasks and Sub-directories as Sub-Task
  * Blobs not implemented for version 1, can be used as attachments for version 2
  * tags - not implemented
  * save - base on git commit
* Storage
  * version 1- git based filesystem
  * version 2- SQLite - lightweight databases
  * version 3 - RestAPI - GitHub Cli

  
* References
  * https://github.com/CodeIntelligenceTesting/cifuzz  
  * GitHub CLI - https://github.com/cli/cli
  * For RestAPI - https://github.com/cli/go-gh 
  * https://github.com/git-lfs/git-lfs
  * https://medium.com/kanoteknologi/better-way-to-read-and-write-json-file-in-golang-9d575b7254f2
  * https://gist.github.com/Integralist/344837ede1d85739fdfa05410db9ffee


* Discussions
  *  Datastructure
  * LinkedList - traversal for large items,with disconnected nodes on edit for immutability
  * Tree - contains latest snapshot
  * Graphs - complex for the task
