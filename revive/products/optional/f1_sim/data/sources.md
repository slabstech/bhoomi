
* List of sources for F1 data

  * F1 championship telemetry data
    * Kaggle - https://www.kaggle.com/rohanrao/formula-1-world-championship-1950-2020](https://www.kaggle.com/rohanrao/formula-1-world-championship-1950-2020)
    * API data - [https://ergast.com/mrd/](https://ergast.com/mrd/)
    * GithubTopics - [https://github.com/topics/ergast-api](https://github.com/topics/ergast-api)


  * F1 2021 game telemetry data
    * Java + Kafka - [https://github.com/ppatierno/formula1-telemetry-kafka](https://github.com/ppatierno/formula1-telemetry-kafka)
    * Java -  [https://github.com/ppatierno/formula1-telemetry](https://github.com/ppatierno/formula1-telemetry)
    * JS - [https://github.com/raweceek-temeletry/f1-2021-udp](https://github.com/raweceek-temeletry/f1-2021-udp)
    * JS - [https://github.com/mrcodedev/f1-2021-telemetry-app](https://github.com/mrcodedev/f1-2021-telemetry-app)
    * JS - [https://github.com/racehub-io/f1-telemetry-client](https://github.com/racehub-io/f1-telemetry-client)
