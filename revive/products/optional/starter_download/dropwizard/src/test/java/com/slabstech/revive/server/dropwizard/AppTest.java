package com.slabstech.revive.server.dropwizard;

import com.slabstech.revive.server.dropwizard.representations.Contact;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


public class AppTest {

    private Client client;

    private Contact contactForTest = new Contact("Jane", "+123456");

    @ClassRule
    public static final DropwizardAppExtension<AppConfiguration> RULE = new DropwizardAppExtension<>(App.class, "conifg.yml");

    // Add Authenticatication filter
    @Before
    public void setUp(){
       // client = new Client();

    }

    @Test
    public void createAndRetreiveContact(){

        WebTarget webTarget = client.target("http://localhost:8080/contact/" );
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.put(Entity.entity(contactForTest, MediaType.APPLICATION_JSON));

        assertThat(response.getStatus()).isEqualTo(201);


        // Retreive created contract
        String newContactUrl = (String) response.getHeaders().getFirst("Location");

        WebTarget newWebTarget = client.target(newContactUrl);
        Invocation.Builder newInvocationBuilder =  newWebTarget.request(MediaType.APPLICATION_JSON);
        Response newResponse = invocationBuilder.get();

        Contact contact = newResponse.readEntity(Contact.class);

        assertThat(contact.getName()).isEqualTo(contactForTest.getName());

        assertThat(contact.getPhone()).isEqualTo(contactForTest.getPhone());




    }
}
