package com.slabstech.revive.server.dropwizard.resources;

import com.slabstech.revive.server.dropwizard.dao.ContactDAO;
import com.slabstech.revive.server.dropwizard.representations.Contact;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

@Path("/contact")
@Produces(MediaType.APPLICATION_JSON)
public class ContactResource {

    private final ContactDAO contactDAO;

    private final Validator validator;

    public ContactResource(ContactDAO contactDAO, Validator validator) {

        this.contactDAO = contactDAO;
        this.validator = validator;
    }

    @GET
    @UnitOfWork
    @Path("/{id}")
    public Response getContact(@PathParam("id") long id){
        // Retreive information aabout Contact from provided Id
        Optional<Contact> contact = contactDAO.findById(id);

        return Response
                .ok(contact)
                .build();
    }

    @POST
    @UnitOfWork
    public Response createContact(Contact contact) throws URISyntaxException {

        //Validate the contact dats
        Set<ConstraintViolation<Contact>> violations = validator.validate(contact);

        if(violations.size()>0){
            ArrayList<String> validationMessages = new ArrayList<>();
            for(ConstraintViolation<Contact>violation:violations){
                validationMessages.add(violation.getPropertyPath().toString() + ": " + violation.getMessage());
            }
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(validationMessages)
                    .build();
        }else{

            long contactId = contactDAO.create(contact.getName(), contact.getPhone()).getId();
            return Response
                    .created(new URI(String.valueOf(contactId)))
                    .build();
        }

    }

    //TODO Add delete function

    @DELETE
    @Path("/{id}")
    @UnitOfWork
    public Response deleteContact(@PathParam("id") int id){
        return Response
                .noContent()
                .build();
    }

    @PUT
    @Path("/{id}")
    @UnitOfWork
    public Response updateContact(@PathParam("id") int id, Contact contact) throws URISyntaxException {

        //Validate the contact dats
        Set<ConstraintViolation<Contact>> violations = validator.validate(contact);

        if(violations.size()>0){
            ArrayList<String> validationMessages = new ArrayList<>();
            for(ConstraintViolation<Contact>violation:violations){
                validationMessages.add(violation.getPropertyPath().toString() + ": " + violation.getMessage());
            }
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(validationMessages)
                    .build();
        }else{
            long contactId = contactDAO.update(contact.getId(), contact.getName(), contact.getPhone()).getId();
            return Response
                    .ok(new URI(String.valueOf(contactId)))
                    .build();
        }

    }
}