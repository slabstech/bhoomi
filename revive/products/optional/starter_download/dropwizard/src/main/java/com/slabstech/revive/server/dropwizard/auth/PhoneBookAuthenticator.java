package com.slabstech.revive.server.dropwizard.auth;

import com.slabstech.revive.server.dropwizard.representations.Contact;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

import java.util.Optional;

public class PhoneBookAuthenticator implements Authenticator <BasicCredentials, Contact>{
    @Override
    public Optional<Contact> authenticate(BasicCredentials credentials) throws AuthenticationException {

        if(credentials.getUsername() == "john_doe" && credentials.getPassword() == "secret")
            return Optional.of(new Contact("ss", "ss"));

        return Optional.empty();
    }
}
