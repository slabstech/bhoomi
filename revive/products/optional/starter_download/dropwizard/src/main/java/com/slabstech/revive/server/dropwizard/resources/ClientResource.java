package com.slabstech.revive.server.dropwizard.resources;


import com.slabstech.revive.server.dropwizard.representations.Contact;
import com.slabstech.revive.server.dropwizard.views.ContactView;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;


@Path("/client/")
@Produces(MediaType.TEXT_HTML)
public class ClientResource {
    private Client client;

    public ClientResource(Client client){
        this.client = client;
    }

    @GET
    @Path("showContact")
    public ContactView showContact(@QueryParam("id") long id){

        WebTarget webTarget = client.target("http://localhost:8080/contact/" + id);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        Contact contact = response.readEntity(Contact.class);
        return new ContactView(contact);
    }

    @GET
    @Path("deleteContact")
    public Response deleteContact(@QueryParam("id") long id){
        WebTarget webTarget = client.target("http://localhost:8080/contact/" + id);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.delete();
        return Response.noContent().entity("Contact was deleted").build();
    }

    @GET
    @Path("newContact")
    public Response newContact(@QueryParam("name") String name, @QueryParam("phone") String phone){
        WebTarget webTarget = client.target("http://localhost:8080/contact/");
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(new Contact(name,phone), MediaType.APPLICATION_JSON));

        if(response.getStatus() == 201)
            return Response.status(302).entity("Contact was created successfully, new Entity can be found at : "
                    + response.getHeaders().getFirst("Location"))
                    .build();
        else
            return Response.status(422).entity(response.getEntity()).build();
    }

    @GET
    @Path("updateContact")
    public Response updateContact(@QueryParam("id") long id, @QueryParam("name") String name, @QueryParam("phone") String phone){
        WebTarget webTarget = client.target("http://localhost:8080/contact/" + id);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.put(Entity.entity(new Contact(id,name,phone), MediaType.APPLICATION_JSON));

        if(response.getStatus() == 200)
            return Response.status(302).entity("Contact was updated successfully").build();
        else
            return Response.status(422).entity(response.getEntity()).build();
    }



}
