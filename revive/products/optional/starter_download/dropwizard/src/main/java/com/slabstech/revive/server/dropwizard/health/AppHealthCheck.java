package com.slabstech.revive.server.dropwizard.health;

import com.codahale.metrics.health.HealthCheck;
import com.slabstech.revive.server.dropwizard.representations.Contact;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AppHealthCheck extends HealthCheck {

    private final Client client;

    public AppHealthCheck(Client client){
        super();
        this.client = client;
    }

    @Override
    protected Result check() throws Exception{

            WebTarget webTarget = client.target("http://localhost:8080/contact/");
            Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
            Response response = invocationBuilder.put(Entity.entity(new Contact("Health Check Name", "+123123"), MediaType.APPLICATION_JSON));

            if(response.getStatus() == 201)
                return Result.healthy();
            else
                return Result.unhealthy("Contact Cannot be created");


    }
}
