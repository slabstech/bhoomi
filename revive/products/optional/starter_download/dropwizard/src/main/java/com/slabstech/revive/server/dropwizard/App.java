package com.slabstech.revive.server.dropwizard;

import com.slabstech.revive.server.dropwizard.dao.ContactDAO;
import com.slabstech.revive.server.dropwizard.health.AppHealthCheck;
import com.slabstech.revive.server.dropwizard.representations.Contact;
import com.slabstech.revive.server.dropwizard.resources.ClientResource;
import com.slabstech.revive.server.dropwizard.resources.ContactResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;


public class App extends Application<AppConfiguration>{
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);


    @Override
    public void initialize(Bootstrap <AppConfiguration> bootstrap){
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );

        bootstrap.addBundle(new MigrationsBundle<AppConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(AppConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        bootstrap.addBundle(hibernateBundle);

        bootstrap.addBundle( new ViewBundle());
        bootstrap.addBundle( new AssetsBundle());

    }


    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public void run(AppConfiguration configuration, Environment environment) throws Exception {
        System.out.println("Hello World, by Dropwizard");
        LOGGER.info("Method App#run() called");

        for (int i = 0; i < configuration.getMessageRepetitions(); i++) {
            System.out.println(configuration.getMessage());
        }

        final ContactDAO contactDAO = new ContactDAO(hibernateBundle.getSessionFactory());
        environment.jersey().register(new ContactResource(contactDAO, environment.getValidator()));

        // build the client
        final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration())
                        .build("Http RestAPI");
        environment.jersey().register(new ClientResource(client));

        environment.healthChecks().register("New ContactHealthCheck", new AppHealthCheck(client));

    }

    private final HibernateBundle<AppConfiguration> hibernateBundle =
            new HibernateBundle<AppConfiguration>(Contact.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(AppConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

}
