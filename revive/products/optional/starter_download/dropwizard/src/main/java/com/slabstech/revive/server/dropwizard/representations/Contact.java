package com.slabstech.revive.server.dropwizard.representations;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.*;
import java.security.Principal;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

@Entity
@Table(name = "contact")
@NamedQuery(
        name = "com.slabstech.revive.server.dropwizard.representations.Contact.findAll",
        query = "SELECT c FROM Contact c"
)
public class Contact implements Principal{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private final long id;


    @NotBlank
    @Length(min = 2, max = 255)
    @Column(name = "name", nullable = false)
    private final String name;

    @NotBlank
    @Length(min = 2, max = 30)
    @Column(name = "phone", nullable = false)
    private final String phone;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public Contact() {
        this.id = 0;
        this.name = null;
        this.phone = null;
    }

    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone;
        this.id =0;
    }

    public Contact(long id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }
}
