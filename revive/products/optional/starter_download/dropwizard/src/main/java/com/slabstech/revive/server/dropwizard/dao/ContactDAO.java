package com.slabstech.revive.server.dropwizard.dao;

import com.slabstech.revive.server.dropwizard.representations.Contact;

import io.dropwizard.hibernate.AbstractDAO;

import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;


public class ContactDAO extends AbstractDAO<Contact> {

    public ContactDAO(SessionFactory factory) {
        super(factory);
    }

        public Optional<Contact> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

        public Contact create(Contact user) {
        return persist(user);
    }

        public List<Contact> findAll() {
        return list(namedTypedQuery("com.slabstech.revive.server.dropwizard.representations.Contact.findAll"));
    }

    public Contact create(String name, String phone) {
        Contact contact= new Contact(name, phone);
        return persist(contact);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public Contact update(long id, String name, String phone) {
        Contact contact= new Contact(id, name, phone);
        return persist(contact);

    }


}
