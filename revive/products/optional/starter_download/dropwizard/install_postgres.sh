* Install steps 

* Create Setup
    * create database dropwizard_db;
    * create user dropwizard_db with password 'dropwizard_db';
    * grant all privileges on database dropwizard_db to dropwizard_db;
* To login
    * psql -h localhost -p 5432 -d dropwizard_db -U dropwizard_db
* To display all tables
    * \dt

* Execute the schema_contact.sql