* To build Project
  * gradle build

* To Run Program
  * java -jar build/libs/app-0.0.1.jar server config.yml
  * gradle runDropwizard

* To insert value via CURL
  * curl --verbose --header "Content-Type: application/json" -X POST -d '{"name" : "FOO", "phone" : "+456789"}' http://localhost:8080/contact

* Client
  * GET
    * http://localhost:8080/client/showContact?id=1
  * POST
    * http://localhost:8080/client/newContact?name=Sachin&phone=+741852 
  * PUT
    * http://localhost:8080/client/updateContact?id=6&name=Sachin&phone=+742
  * DELETE
    * 

* To update value
  * TODO -  find command to add as contact



* Reference
  * @Valid can also be called on the createContact function as
    * public Response createContact(@Valid Contact contact)