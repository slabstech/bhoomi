# Micro-services Starter Example

[![Build](https://github.com/slabstech/revive_starter_download/actions/workflows/branch.yml/badge.svg)](https://github.com/slabstech/revive_starter_download/actions/workflows/branch.yml)

Collection of starter projects with Dockerfiles to learn Micro-services

* Dropwizard + PostgreSQL + Gradle


| Tutorial for Docker Gradle based Projects | Status |
|---|---|
|[Spring Boot CLI with PostgreSQL in Docker](https://slabstech.github.io/docker_gradle_tutorial/)|WIP|
|[Spring Boot + Thymeleaf with PostgreSQL in Docker](https://slabstech.github.io/docker_gradle_tutorial/)| WIP |
|[Spring Boot + ReactJS with PosgreSQL in Docker](https://slabstech.github.io/docker_gradle_tutorial/)| WIP |
|[Spring Boot + ReactJS Multi-service with PosgreSQL in Docker](https://slabstech.github.io/docker_gradle_tutorial/)| WIP |



ref
https://github.com/bszeti/dropwizard-dwexample/blob/master/dwexample-client/src/main/java/bszeti/dw/example/client/ServiceClient.java
https://www.programcreek.com/java-api-examples/?class=javax.ws.rs.client.Invocation.Builder&method=post