package com.slabstech.revive.lakshmi.ui.dao;

import com.slabstech.revive.lakshmi.ui.entities.Company;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface  CompanyDAO extends CrudRepository<Company, UUID> {
}
