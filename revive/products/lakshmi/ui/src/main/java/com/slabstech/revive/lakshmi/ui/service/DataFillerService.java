package com.slabstech.revive.lakshmi.ui.service;

import com.slabstech.revive.lakshmi.ui.dao.CompanyDAO;
import com.slabstech.revive.lakshmi.ui.dao.IndexesDAO;
import com.slabstech.revive.lakshmi.ui.entities.Company;
import com.slabstech.revive.lakshmi.ui.entities.Indexes;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service
public class DataFillerService {
    private final CompanyDAO companyDAO;
    private final IndexesDAO indexesDAO;
    public DataFillerService(CompanyDAO courseDAO, IndexesDAO teacherDAO) {
        this.companyDAO = courseDAO;
        this.indexesDAO = teacherDAO;
    }
    @PostConstruct
    @Transactional
    public void fillData(){
        Indexes pj = new Indexes(
                "Professor Jirafales",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Ruben2017.jpg/245px-Ruben2017.jpg",
                "jirafales@yahoo_.com", 1L
        );
        Indexes px = new Indexes(
                "Professor X",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9uI1Cb-nQ2uJOph4_t96KRvLSMjczAKnHLJYi1nqWXagvqWc4",
                "director@xproject_.com", 2L

        );
        indexesDAO.save(pj);
        indexesDAO.save(px);
        // create courses
        Company mathematics = new Company("Mathematics", 20, (short) 10, pj,1L);
        Company spanish = new Company("Spanish", 20, (short) 10, pj,2L);
        Company dealingWithUnknown = new Company("Dealing with unknown", 10, (short) 100, pj, 3L);
        Company handlingYourMentalPower = new Company("Handling your mental power", 50, (short) 100, pj, 4L);
        Company introductionToPsychology = new Company("Introduction to psychology", 90, (short) 100, pj,5L);
        companyDAO.save(mathematics);
        companyDAO.save(spanish);
        companyDAO.save(dealingWithUnknown);
        companyDAO.save(handlingYourMentalPower);
        companyDAO.save(introductionToPsychology);
    }
}
