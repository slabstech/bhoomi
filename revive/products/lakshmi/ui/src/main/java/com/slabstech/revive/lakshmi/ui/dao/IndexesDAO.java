package com.slabstech.revive.lakshmi.ui.dao;

import com.slabstech.revive.lakshmi.ui.entities.Indexes;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface IndexesDAO extends CrudRepository<Indexes, UUID> {
}
