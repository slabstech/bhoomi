package com.slabstech.revive.lakshmi.ui.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@org.springframework.stereotype.Controller
@RequestMapping("/")
public class Controller {
    private final RestTemplate restTemplate;
    private final String serviceHost;

    public Controller(RestTemplate restTemplate, @Value("${service.host}") String serviceHost) {
        this.restTemplate = restTemplate;
        this.serviceHost = serviceHost;
    }

    @RequestMapping("")
    public ModelAndView index(){
        return new ModelAndView("index");
    }

    @GetMapping("/indexes")
    public ResponseEntity<List<IndexesDto>> listIndexes(){
        return restTemplate.exchange("http://" + serviceHost +
                "/class", HttpMethod.GET, null, new ParameterizedTypeReference<List<IndexesDto>>() {
        });
    }
}
