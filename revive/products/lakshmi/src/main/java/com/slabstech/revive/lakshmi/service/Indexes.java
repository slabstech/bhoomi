package com.slabstech.revive.lakshmi.service;

import org.locationtech.jts.geom.Geometry;

public record Indexes(String name, Geometry area) {}
