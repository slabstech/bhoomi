package com.slabstech.revive.lakshmi.api;

public class IndexesNotFoundException extends RuntimeException {

    public IndexesNotFoundException() {
        super();
    }

    public IndexesNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public IndexesNotFoundException(final String message) {
        super(message);
    }

    public IndexesNotFoundException(final Throwable cause) {
        super(cause);
    }
}
