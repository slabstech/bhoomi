package com.slabstech.revive.lakshmi.service;

import com.slabstech.revive.lakshmi.repository.IndexesRepository;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

@RequiredArgsConstructor
public class IndexesService {

    private final @NonNull GeometryFactory geometryFactory;
    private final @NonNull CoordinateTransformationService cts;
    private final @NonNull IndexesRepository repo;

    @Transactional
    public Indexes createForLocation(
            String name, double latitude, double longitude, double radiusInKM) {

        Point location = geometryFactory.createPoint(new Coordinate(longitude, latitude));

        double radiusInDegrees = cts.metersToDegrees(radiusInKM * 1_000);

        Geometry area = location.buffer(radiusInDegrees);

        com.slabstech.revive.lakshmi.repository.Indexes entity =
                new com.slabstech.revive.lakshmi.repository.Indexes();
        entity.setName(name);
        entity.setArea(area);
        entity = repo.save(entity);

        return toModel(entity);
    }

    public Optional<Indexes> findByName(@NonNull String name) {
        return Optional.ofNullable(repo.findByName(name)).map(this::toModel);
    }

    public List<Indexes> findAll() {
        return repo.findAllByOrderByName().stream().map(this::toModel).toList();
    }

    private Indexes toModel(com.slabstech.revive.lakshmi.repository.Indexes entity) {
        return new Indexes(entity.getName(), entity.getArea());
    }
}
