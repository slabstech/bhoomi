package com.slabstech.revive.lakshmi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IndexesRepository extends JpaRepository<Indexes, Long> {

    Indexes findByName(String name);

    List<Indexes> findAllByOrderByName();
}
