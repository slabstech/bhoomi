package com.slabstech.revive.lakshmi.api;

public class IndexesIdMismatchException extends RuntimeException {

    public IndexesIdMismatchException() {
        super();
    }

    public IndexesIdMismatchException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public IndexesIdMismatchException(final String message) {
        super(message);
    }

    public IndexesIdMismatchException(final Throwable cause) {
        super(cause);
    }
}
