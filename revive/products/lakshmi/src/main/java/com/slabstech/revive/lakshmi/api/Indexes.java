package com.slabstech.revive.lakshmi.api;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Indexes {

    private String name;
    private String areaWKT;

    public static @NonNull Indexes valueOf(@NonNull String name, @NonNull String area) {
        return new Indexes().setName(name).setAreaWKT(area);
    }
}
