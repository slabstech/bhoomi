package com.slabstech.revive.lakshmi.repository;

import lombok.Data;
import lombok.experimental.Accessors;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;

@Entity
@Table(name = "indexes")
@Data
@Accessors(chain = true)
public class Indexes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String name;

    private Geometry area;
}
