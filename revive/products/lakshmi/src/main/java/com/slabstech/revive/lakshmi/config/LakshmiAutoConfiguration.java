package com.slabstech.revive.lakshmi.config;

import com.slabstech.revive.lakshmi.api.Indexes;
import com.slabstech.revive.lakshmi.api.IndexesController;
import com.slabstech.revive.lakshmi.repository.IndexesRepository;
import com.slabstech.revive.lakshmi.service.CoordinateTransformationService;
import com.slabstech.revive.lakshmi.service.IndexesService;

import org.locationtech.jts.geom.GeometryFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackageClasses = IndexesRepository.class)
@EntityScan(basePackageClasses = Indexes.class)
public class LakshmiAutoConfiguration {

    public @Bean IndexesController planAreasController(IndexesService service) {
        return new IndexesController(service);
    }

    @Bean
    IndexesService planAreaService(
            GeometryFactory gFac, CoordinateTransformationService cts, IndexesRepository repo) {
        return new IndexesService(gFac, cts, repo);
    }

    @Bean
    GeometryFactory geometryFactory() {
        return new GeometryFactory();
    }

    @Bean
    CoordinateTransformationService coordinateTransformationService() {
        return new CoordinateTransformationService();
    }
}
