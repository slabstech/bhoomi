package com.slabstech.revive.lakshmi.index

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LakshmiApplication

fun main(args: Array<String>) {
	runApplication<LakshmiApplication>(*args)
}
