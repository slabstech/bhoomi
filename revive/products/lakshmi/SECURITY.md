### Security

* Secure by Design
* Data Encrypted at Rest
* Use OKTA for authentication
  * Test supertokens 
* Build Layered access like onion for fetch and insert
* Log analysis
* Daily Backups
  * 