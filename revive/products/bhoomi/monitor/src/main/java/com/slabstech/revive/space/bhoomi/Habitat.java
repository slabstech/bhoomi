package com.slabstech.revive.space.bhoomi;

import java.util.ArrayList;
import java.util.List;

public class Habitat {

    List<java.lang.Module> moduleList = new ArrayList<>();
    Integer missionDays ;

    Habitat(){
        baseConfig();
    }

    Habitat(int missionDays){
        this.missionDays = missionDays;
        baseConfig();
        if(missionDays<=30){
            moduleList.addAll(initialiseShortTermConfiguration());

        } else if (missionDays <=60) {
            moduleList.addAll(initialiseLongTermConfiguration());
        }
    }

    private  List<java.lang.Module> initialiseLongTermConfiguration() {
        return null;
    }

    private  List<java.lang.Module> initialiseShortTermConfiguration() {
        return null;
    }


    private void baseConfig() {
    }
}
