package com.slabstech.revive.space.bhoomi;

public enum AreaType {
    Clean, Dirty;
}
