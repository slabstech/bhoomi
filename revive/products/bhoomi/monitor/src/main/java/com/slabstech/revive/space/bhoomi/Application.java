package com.slabstech.revive.space.bhoomi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication

public class Application {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        System.out.println("Template Library for RESTFul Web Apps with Micro-services");

        SpringApplication.run(Application.class, args);
    }
}