package com.slabstech.revive.space.bhoomi;

public class Dimension {
    double metricLength;
    double metricWidth;
    double metricHeight;

    Dimension(double width, double length, double height)
    {
        this.metricWidth = width;
        this.metricHeight = height;
        this.metricLength = length;
    }
}
