## Bhoomi

## Oct 3 Update 
* Project proposal implemented at [https://github.com/slabstech/bhoomi](https://github.com/slabstech/bhoomi)
* Current Demo - [https://mangala.earth/](https://mangala.earth/)
* Greenhouse Earth Simulation Data - [ಕಲ್ಪವೃಕ್ಷ](https://mangala.earth/greenhouse/pre-process/)
* Habitat Designs Generated at - [Build](https://mangala.earth/habitat/build/) 

To be tested on
* Hardware - 
  * Developement - Raspberry pi 3b
  * Test - Raspberry pi Zero
  * Deployment - Raspberry Pi Pico
* Software -  
  * OS - windriver/wrlx-image:minimal
  * Programming Langzage - C++

* Space Agency Data
  * DLR
    * MarsExpress : HRSC Camera 
  * ISRO - 
    * Science Data : https://www.isro.gov.in/Sciencedata.html
    * Mars Atlas : https://www.mosdac.gov.in/flip-book/demos/mars.html
    * Mars Orbiter : https://www.isro.gov.in/MarsOrbiterMissionSpacecraft.html
    * Mars Orbiter Publications : https://www.isro.gov.in/media_isro/pdf/Publications/Sciencedata/mars_orbiter_mission_mom_-_list_of_publications-october2020.pdf
    * Mars : Illustrated Book - https://www.isro.gov.in/media_isro/pdf/Publications/Sciencedata/Illustrated%20Book%20on%20Mars%20Orbiter%20Mission.pdf
    * Exploratory Science _ https://www.sac.gov.in/Vyom/ExploratorySciences.jsp


* Steps
  * https://github.com/in28minutes/deploy-spring-boot-to-azure
  * https://github.com/microsoft/azure-maven-plugins/blob/develop/azure-webapp-maven-plugin/README.md
  * https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-cli
  * https://learn.microsoft.com/en-us/cli/azure/
  * Pricing Tier : https://azure.microsoft.com/en-us/pricing/details/app-service/linux/
  * https://learn.microsoft.com/en-us/azure/app-service/quickstart-java?tabs=javase&pivots=platform-linux-development-environment-maven
  * https://github.com/spring-guides/gs-spring-boot-for-azure
  * https://learn.microsoft.com/en-us/azure/spring-apps/how-to-github-actions?pivots=programming-language-java
  * https://learn.microsoft.com/en-us/azure/spring-apps/github-actions-key-vault
  * https://github.com/MicrosoftDocs/azure-docs/blob/main/articles/api-management/get-started-create-service-instance-cli.md
  * https://github.com/MicrosoftDocs/azure-docs-cli/blob/main/docs-ref-conceptual/use-cli-effectively.md
  * https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle/
  * 

* Commands
  * az login
  * mvn com.microsoft.azure:azure-webapp-maven-plugin:2.2.0:config
  * mvn azure-webapp:deploy
  * az login && az ad sp create-for-rbac --role contributor --scopes /subscriptions/203a4837-8ad8-43b6-a303-cda7f02e4ae0 --sdk-auth
  * az extension add --name spring
  * az group create --location centralus --name bhoomi-monitor-1664733866042-rg
  * az spring create -n bhoomi-monitor-1664733866042 -g bhoomi-monitor-1664733866042-rg
  * az spring config-server git set -n bhoomi-monitor-1664733866042 --uri https://github.com/slabstech/revive --label config -g bhoomi-monitor-1664733866042-rg
  * 
  * az group delete --name bhoomi-monitor-1664733866042-rg --yes