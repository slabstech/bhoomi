# nakshe
VR Comic Engine

* Engine generates VR Game from Comic Book

* Usage
  * Input : pdf book
  * Output
    * Txt file for screen play
    * images for screen art
    * flow chart for timeline

* How
  * Convert Comic books to VR Game
    * Extract Text Using OCR
    * Translate Text to target language using Google Translate
    * Predict Next Dialogue using Stanford NLP
    * Generate Screen Art using Reinforcement Learning
    * Connect ScreenPlay or generate alternate timeline like Black Mirror

* Audience
  * Marvel / DC
  * Amar Chitra Katha, Anant Pai
  * Indie Publishers


* Monetisation
  * Pre and Post Production Support
  * Automation and Infrastructure Support


* Sample Book : Gaganyatri
  * Book
  * Comic
  * Browser Game
  * VR Game


# Text Extraction

* All Processing will be done on the Browser

* Use Tensorflow Js for model extraction 

* No Copyright issues due to local processing

*

