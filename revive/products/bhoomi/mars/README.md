### Mars Habitat Challenge

* 3d models
  * Perseverance Rover - https://nasa3d.arc.nasa.gov/detail/M2020-Model-Rover-STLs512020
  * Rover Wheel - https://nasa3d.arc.nasa.gov/detail/Mini-Perseverance-Rover-Wheel
  * Sample tube - https://nasa3d.arc.nasa.gov/detail/Mars-2020-Sample-Tube-3D-print-files
  * Transit Habitat - https://nasa3d.arc.nasa.gov/detail/mars_transit_habitat 
  
* Simulated Mission
  * https://www.nasa.gov/chapea

* Construction
  * Project Olympus -  https://ttu-ir.tdl.org/handle/2346/87095
  * Lunar Excavation & Construction - https://www.youtube.com/watch?v=SwFcwEBIiEQ
  * Additive manufacturing - https://www.nist.gov/additive-manufacturing
  
* Phoenix Mission Data
  * Meteorological - https://donnees-data.asc-csa.gc.ca/dataset/0e338f52-25e4-465c-8104-bb9d7c8d3df3
  * Terrain Emulation - https://donnees-data.asc-csa.gc.ca/dataset/65376529-3z6l-6u7e-732sbzy824wa25
  
* https://github.com/nasa-jpl/open-source-rover
* https://github.com/nasa-jpl/osr-rover-code


