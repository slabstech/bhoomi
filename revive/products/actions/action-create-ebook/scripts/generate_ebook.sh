#!/bin/bash

cp order.txt order_html.txt
sed -i 's/md/html/g' order_html.txt
FILE=`cat order.txt`
for f in $FILE; do grip --export $f --no-inline; done

rm -rf .book
mkdir .book
mv order_html.txt .book
mv *.html .book
cd .book

HTML_FILE=`cat order_html.txt`
cat $HTMLFILE  > book.html

wkhtmltopdf book.html book.pdf
cp .book/book.pdf ..

