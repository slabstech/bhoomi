# [Connectthedots](https://slabstech.com/connectthedots.com/)

[![Build](https://github.com/slabstech/connectthedots.com/actions/workflows/pages/pages-build-deployment/badge.svg)](https://github.com/slabstech/connectthedots.com/actions/workflows/pages/pages-build-deployment)

### Art Portfolio site by Suma Shetty


#### Site powered by [S Labs Foundation](https://slabstech.com/art/)


Thanks to Michael Rose for the [Minimal Mistakes Jekyll Plugin](https://mmistakes.github.io/minimal-mistakes/)