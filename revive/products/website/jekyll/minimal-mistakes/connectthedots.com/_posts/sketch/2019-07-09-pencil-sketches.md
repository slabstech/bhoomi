---
title: "Sketches : Set 1"
categories:
  - sketch
tags:
  - sketch
---

<h3 style="text-align: center;">Village Girl</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-01.jpg" alt="Village Girl" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Party</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-02.jpg" alt="Party" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Birthday</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-03.jpg" alt="Birthday" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Child</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-04.jpg" alt="Child" style="border: 5px solid #505">
<br/><br/>

<h3 style="text-align: center;">Girl Laughs</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-05.jpg" alt="Girl Laughs" style="border: 5px solid #505">
<br/><br/>

<h3 style="text-align: center;">Women</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-06.jpg" alt="Women" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Beach</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-07.jpg" alt="Beach" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Valley</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-08.jpg" alt=Valley" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Vase -  Etched</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-09.jpg" alt="Vase - Etched" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Vase - Smudge</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-10.jpg" alt="Vase - Smudge" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Owl</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-11.jpg" alt="Owl" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Kittens</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-12.jpg" alt="Kittens" style="border: 5px solid #555">

<br/><br/>

<h3 style="text-align: center;">Horse</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-13.jpg" alt="Horse" style="border: 5px solid #555">
<br/><br/>

<h3 style="text-align: center;">Ganapathi</h3>
<br/><br/>
<img src="{{site.baseurl}}/assets/art/sketch/set1/sketch-14.jpg" alt="Ganapathi" style="border: 5px solid #555">