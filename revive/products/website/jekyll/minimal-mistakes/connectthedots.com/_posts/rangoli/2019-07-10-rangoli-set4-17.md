---
title: "Set 3 : Chariot and Arcs"
categories:
  - rangoli
tags:
  - rangoli
---

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-01.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-02.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-03.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-04.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-08.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-12.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-15.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set4/set-4-17.jpg" style="border: 5px solid #555">


<img src="{{site.baseurl}}/assets/art/rangoli/set5/set-5-12.jpg" style="border: 5px solid #555">


<img src="{{site.baseurl}}/assets/art/rangoli/set5/set-5-13.jpg" style="border: 5px solid #555">


<img src="{{site.baseurl}}/assets/art/rangoli/set6/set-6-06.jpg" style="border: 5px solid #555">


<img src="{{site.baseurl}}/assets/art/rangoli/set6/set-6-16.jpg" style="border: 5px solid #555">

<img src="{{site.baseurl}}/assets/art/rangoli/set6/set-6-17.jpg" style="border: 5px solid #555">
