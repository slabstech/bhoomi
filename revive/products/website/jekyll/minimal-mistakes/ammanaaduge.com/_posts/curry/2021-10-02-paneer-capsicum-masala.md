---
title: "CPaneer Capsicum Masala: Curry"
categories:
  - curry
tags:
  - curry
youtubeId: i6EV7bBXL8g
---

{% include youtubePlayer.html id=i6EV7bBXL8g %}



| Servings | Cooking Time | Cost |
|-------|-------|-------|
| 5 people | 60 mins | Rs. 200|


Paneer Capsicum Masala Recipe

Instagram: https://www.instagram.com/sahanashett...

Servings: 6 to 7
Cooking time: 60 minutes
Cost: 200 Rs (approximately)

Ingredients :
• Paneer - 200 gm
• Capsicum(medium) - 4
• Onion (Large) - 4
• Tomato - 3
• Ginger garlic paste - 2 tbsp
• Red chilli powder - 1 tbsp
• Turmeric powder - 1 tsp
• Jeera (Cumin seeds) - 1 tbsp
• Garam masala powder - 1 tsp
• Fresh cream - 50 gm
• Oil - 4 tbsp
• Salt – 1 tbsp
• Kasuri methi – 2 tbsp

Preparation :
1. Add oil in a kadhai(wok). Put finely chopped onions and then jeera to it.
2. Once the onion turns golden brown, put tomato.
3. Add cashew nuts and cook it for a while.
4. Now add ginger garlic paste.
5. Once the pungent smell of ginger garlic paste is gone, add fresh cream.
6. Cook it till the strong smell of fresh cream is gone.
7. Add red chilli powder and turmeric powder and mix it.
8. Cool the mixture, then grind the ingredients adding little bit of water into a fine paste.
9. In a kadhai(wok), add  finely chopped capsicum and saute it.
10. Now add paneer and saute for a while.
11. Put the grinded paste to this, adding bit of water and cook it.
12. Add salt to taste.
13. Now add garam masala powder.
14. Lastly, add kasuri methi and mix the ingredients thoroughly.
15. Restaurant style paneer capsicum masala is ready to it.
