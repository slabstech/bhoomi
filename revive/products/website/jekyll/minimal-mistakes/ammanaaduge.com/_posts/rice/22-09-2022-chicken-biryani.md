---
title: "Chicken Biryani: Homemade chicken"
categories:
- rice
  tags:
- chicken biryani
  youtubeId: 04UCFR1TKzM
---
{% include youtubePlayer.html id=04UCFR1TKzM %}

Instagram: [sahana_shetty](https://www.instagram.com/sahanashetty_ig/)

Servings: 5
Cooking time: 90 minutes
Cost: 300 Rs (approximately)

Ingredients:
• Onion – 3
• Green chilli – 2
• Coriander (chopped) – 2 tbsp
• Pudina or mint leaves (chopped)– 2 tbsp
• Chicken – 750 grams
• Lemon juice – 2 tsp(teaspoon)
• Oil – 10 tbsp
• Curd - 1 bowl/cup
• Salt – 1 tbsp
• Kasuri methi – 1 tbsp
• Garam masala powder - 1 tsp
• Red chilli powder - 2 tbsp
• Turmeric powder - 1 tbsp
• Shahi biriyani masala powder - 1 tbsp
• Ginger garlic paste - 2 to 3 tbsp
• Basmati Rice - 750 gm
• Shah jeera - 2 tsp
• Cinnamon - 2
• Cardamom - 4
• Clove - 6
• Allspice leaves -  3
• Biriyani leaves - 2
• Star anise - 1 or 2

Preparation:

Marination:
1. Take 750 gm of chicken in a bowl. Add salt, red chilli powder, turmeric powder, shahi biriyani masala powder, garam masala powder.
2. Now add curd and Ginger-garlic paste to it.
3. Then add kasuri methi, chopped coriander and mint leaves , lemon juice to the chicken.
4. Mix all the ingredients thoroughly.

Cooking Rice:
1. Take 3 to 4 cups of water in a steel vessel and let it boil.
2. Add shah jeera, oil and salt to it.
3. Now add basmati rice.
4. Let the rice be cooked until it is 75% done.
5. Then drain the water from the rice.

Preparing Biriyani:
1. Add 4 to 5 tbsp oil in a vessel.
2. Now add star anise, cardamom, cinnamon, allspice leaf, biriyani leaves to it.
3. Add marinated chicken and mix all the ingredients.
4. Add green chilli to it.
5. Let the chicken cook for half an hour.
6. Then add 75% cooked basmati rice to it.
7. Let it cook for another half an hour or until the chicken pieces are cooked properly.

Fried onions:
1. Add oil in a frying pan.
2. Now, add onions to it.
3. Saute them until it turns golden brown.
   4 . Now add the fried onion to the rice.
5. The  Chcken Biriyani is now ready to be served.