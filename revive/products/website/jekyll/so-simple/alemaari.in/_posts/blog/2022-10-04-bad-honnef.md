---
layout: post
title: "Bad Honnef"
excerpt: "Must See places"
categories: blog
tags: [ blog ]
date: 2022-10-04T08:08:50-04:00

---

* Landmark
  * [S Labs](https://goo.gl/maps/sDhLQ1ksNJohWrmu9) - Hauptstr 65, 53604 Bad Honnef 


* Activities
  * Buy Dark Light, Connect the Dots, Donated Books at [AWO Store](https://awo.org/), Social Service Organisation.
  * Eat Schoko Croissant at [Welsch Baeckerei](https://baeckerei-welsch.de/)
  * Borrow books from [Samruddhi](https://slabstech.com/samrudhi/)


* Leisure
  * Breath and Rest at [Insel Grafenwerth](https://meinbadhonnef.de/tourismus-freizeit/insel-grafenwerth/)  
    * River bank, Park with sports facilities
  * Reitersdorf park : 3D Chess - play with pieces on the ground
  * Konrad Adenauer House at Rhondorf


* Reaching Bad Honnef 
  1. 16/63 to Hbf
  2. 66 to konrad Adenauer platz /  62 tram to Beuel bahnof
  4. From Beuel Bahnof - Re8 or rb27 to Bad honnef bahnof-  train at 11 and 35 minute hand
  5. Sb51 - asbach bus - 2 stops to IU Hochschule 

  
* Accommodation
  * [Jugendherberge](https://www.jugendherberge.de/jugendherbergen/bad-honnef-435/portraet/): <a href="tel:+49 2224 98981">+49 2224 98981</a > : 50 euro
  * [Maxx](https://hrewards.com/en/maxx-hotel-bad-honnef) : <a href="tel:+4922241890">+49 2224 1890</a >  : 100 euro