---
layout: post
title: "Sponsorship Letter - C type Visa"
excerpt: "For family/friends accomodation "
categories: blog
tags: [ blog ]
date: 2022-09-24T08:08:50-04:00

---

Date : DD/MM/YYYY

From : 

  * Host Name
  * Host Address

To :
    
* The Honorable Consul
  * Address of the Schengen Country Embassy

Subject : Invitation letter for "Applicant Name", Passport no : xxxxx

Your Excellency, Mr. Consul General,

My name is "Host Name", residing at "Host Address". I am employed at "" since "Date".

I am writing this letter to support the application of my "Relationship to host",
"Applicant Name", for a Schengen Visior's visa so he can visit us here in "Schengen Country".

"Applicant Name" will stay in my house on the above address from "dates/duration of stay".
After that he will return to "Applicant Country", where he has a permanent employment at 
"Applicant Employer" as "Applicant's Job Role".

"Applicant Name" will submit this letter to you, along with other required 
documents. Please feel free to contact me on "Host contact information" for queries.

I am hoping for your consideration of his/her Visa Application.

Thank you very much.


Respectfully,
"Name and Signature of Host"


--------

If your accommodation is covered by a person who is living in the country where you are expecting 
to stopover in Schengen Area, then you need to attest to the diplomatic office of the relevant 
country having proof or letter of sponsorship accommodation. The invitee’s letter must have his 
signature and must be approved by the local town hall or an officer.