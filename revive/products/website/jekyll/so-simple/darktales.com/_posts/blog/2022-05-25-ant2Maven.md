---
layout: post
title: "ant2Maven - Convert your ant project to Maven"
excerpt: "Build script for upgrade"
categories: blog
tags: [ blog ]

date: 2022-05-25T08:08:50-04:00

---

With JDK now releasing LTS version every 3 years. It is important to upgrade to benefit from new features and fixing of security issues.

JDK 8 to JDK 11 had a breaking change where certain libraries deprecated and classess were split. 

To test such a scenario we have built a script which reduces time required to upgrade by providing relevent information.

This product has similarity to the dependabot library from Github,

[antMaven link](https://github.com/slabstech/ant2Maven)