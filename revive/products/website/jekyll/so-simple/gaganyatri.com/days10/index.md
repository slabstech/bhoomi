---
layout: page
title: "10 Days"
excerpt: "Countdown to Mankind's Giant Leap"
modified: 2019-07-12T19:44:38.564948-04:00

---

| Project                                                                                                  | Description                                                               | Status |
|----------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------|--------|
| [ಅಮ್ಮ : Amma](https://slabstech.com/amma/)                                                        | Self Aware Replica                                            | WIP    |
| [Revive](https://slabstech.com/revive/)                                                        | Micro Service Template Library                                            | WIP    |
| [Exp56](https://gaganyatri.com/days10/day-02-exp56/)                             | Non Fiction Book- Collection of Poems                                     | Done   |
