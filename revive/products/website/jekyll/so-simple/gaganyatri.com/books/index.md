---
layout: page
title: "Books/ ಕಾದಂಬರಿ ಪುಸ್ತಕಗಳು"
excerpt: "Collections of published books"
---
The stories that mould statues of memories in numerous clay

| Name                | Type      | Download                                                      | Online                                               | Pay                                              | Published Date |
|---------------------|-----------|---------------------------------------------------------------|------------------------------------------------------|--------------------------------------------------|----------------|
| Connecting the Dots | Art       | [PDF](https://gaganyatri.com/assets/pdf/ctd_vol_1.PDF)        | [Read](https://slabstech.com/connectingthedots.com/) | [PDF](https://buy.stripe.com/00g3fZf6E5C27Is8wC) | 19 July 2019   |
| Dark: Light         | Anthology | [PDF](https://gaganyatri.com/assets/pdf/dark_light_vol_1.PDF) | [Read](https://gaganyatri.com/dark_light)            | [PDF](https://buy.stripe.com/aEUeYH2jS1lMfaU3ch) | 19 July 2022   |



P.S - Paperback and Hardbound versions of the books are available on Demand. 

Please contact me for more details.

Contact me at [https://slabstech.com/contact/](https://slabstech.com/contact/)

<!--
| Expedition          | Poem      | [PDF](https://gaganyatri.com/assets/pdf/exp56_vol_1.PDF)      | [Link]({{ site.baseurl }}{% link _posts/books/publish/2019-03-20-exp56.md %}) | [Ebook](https://amzn.to/3N5JcmY)| 7 Jun 2019     |
| Why                 | Novel      | [PDF](https://gaganyatri.com/assets/pdf/why_vol_1.PDF)    | [Link]({{ site.baseurl }}{% link _posts/books/publish/2019-03-20-why.md %})    | [Ebook](https://amzn.to/2PUILxX)  | 22 Dec 2018    |
| ಅಲೆಮಾರಿ / Wandern    | Travelogue | [PDF](https://gaganyatri.com/assets/pdf/travel_vol_1.PDF) | [Link]({{ site.baseurl }}{% link _posts/books/publish/2019-03-29-travel.md %}) | [Ebook](https://amzn.to/312nYzJ)  | 7 Jun 2019     |
-->

<!-- 
Book Rank - 
Dark Light - Sep 15 2022
907,120 in Books
7,371 in Anthologies
425,662 in Foreign Language Fiction
-->
