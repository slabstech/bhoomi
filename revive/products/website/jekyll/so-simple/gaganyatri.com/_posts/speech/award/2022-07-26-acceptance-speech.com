---
layout: post
title: "Award Acceptance Speech"
excerpt: "Turing Best Paper Award"
categories: speech
tags: [ speech ]
date: 2022-07-26T00:00:00-00:00

---

I would like to thank my family and friends for this awards. My father for
sacrificing his best years for childrens education and stability. For teaching me 
to bootstrap projects and learn to do more with less.

My mother for teaching me to be resilient. She always welcomes the new day with hope
and smile even if the previous days were all about taking hits and body blows

My sister for being supportive of my activities and teaching me to be balanced in all path of life.

My friend Deepansh for being a support during the difficult times.

Also to LeanIX and Andre, for giving me a springboard to learn new things and opening 
up vast opportunities