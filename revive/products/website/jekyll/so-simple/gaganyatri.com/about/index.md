---
layout: page
title: About Gaganyatri
excerpt: "Author Info"
modified: 2014-08-08T19:44:38.564948-04:00

---
Short Story :

    Polyglot Programmer, Wanderer , Amateur Author, Bibliophile. 

Long Story :

Hello,  I am Sachin , I love tinkering with anything that can be programmed .

I love reading books all day long and write about the places I visit.

This site is a sneak peek into the work I do and everything else.

* Links to my work and others
  * [Github](https://github.com/sachinsshetty) 
  * [Startup School](https://www.startupschool.org/companies/cN8LeMr9L)
  * [LinkedIn](https://linkedin.com/in/sachinlabs)
  * [Travel Photos](https://instagram.com/alemaari.in)


<!--
About ME !!
* Life long dream to travel in space as an Gaganyatri(Astronaut)
* Bootstrapping a space company to fund ticket and become mission Specialist 
* To Moon, Mars and Beyond. 
* Expect less/no time to any activity unrelated to goal. Long-term absence expected
* Visits art galleries and museum than bars and pubs
* Will prepare a nice meal, visit local places on foot rather than fine dining and clubbing
* Shops at Primark, Decathlon, Tedi, Netto and Non branded items than boutique shopping
* Loves Ink pen, journal and Sticky-notes to prepare and work, not a full digital nomad, but uses the top tools for one's craft
* Lives frugally without indulgence in luxury, No plans to buy Gold, Land to leave behind
* Prefers backpacking, economy flight, youth hostels and trains to First class travel and resorts
* Idols
  * Business - Tim Cook, Sheryl Sandberg, Edison
  * Tech - Linus Torvalds, Larry Page and Sergey Brin, Elon Musk, George Hotz and Aaron Schwartz
  * Leader - Modi, Shri Ram, Krishnadevaraya, Raja Raja Chola
  * Sports - Tendulkar, Federer, Ronaldo
-->
