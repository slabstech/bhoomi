---
layout: post
title: "amma: Self Aware Replica"
excerpt: "Research Paper"
categories: blog
tags: [ blog ]

date: 2022-07-26T08:08:50-04:00

---


 amma : self aware replica
Sachin Shetty
S Labs Solutions

* Abstract
  * Build a decision-making entity based on user data and base rules

* Motivation
  * Harvest the knowledge and experience ove the years to build an engine to provide Top 3 task to do
  * Use the concepts from M.Sc Informatik at Uni-Bonn to create a High Performance Computing Project with Intelligence
  * Build the architecture from the knowledge gained from Professional work and LeanIX best practices
