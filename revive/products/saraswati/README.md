## RubikSolver 

* Algorithm
  * Replication strategy using Tensors

## Execution Time
* https://www.baeldung.com/junit-test-execution-time
### Requirements

* Should execute on a Raspberry Pi 3/4/Zero
* Should be able to solve a 6x3x3 cube in under 10 seconds
* Should use the least amount of memory possible
* Constraint to be verified by running on Docker with a 128MB memory limit

### Expected Output

* Level 1 - A list of moves to solve the cube
* Level 2 - A list of moves to solve the cube in reverse
* Level 3 - A list of moves to solve the cube in reverse, but with the middle layer solved first



### Reference :
* Alpine WSL - Docker Desktop - https://learn.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package
* Java microbenchmark harness
* Sonarqube Build Gate : https://sonarcloud.io/dashboard?id=joel-costigliola_assertj-core
* AssertJ : 
  * https://www.baeldung.com/introduction-to-assertj
  * https://assertj.github.io/doc/

* Codility Examples : https://app.codility.com/programmers/
* https://github.com/slabstech/keeper/blob/sp16-rubiks-cube-solver/puzzle/rubiks_cube/go/README.md

* Project Euler :  https://projecteuler.net/archives , https://projecteuler.net/problem=1

* Iterate over map
  * https://stackoverflow.com/questions/46898/how-do-i-efficiently-iterate-over-each-entry-in-a-java-map