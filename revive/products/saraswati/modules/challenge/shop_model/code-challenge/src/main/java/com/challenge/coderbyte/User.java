package com.challenge.coderbyte;

public class User {

    String name;
    String address;

    public void buyProduct(long productArticleNumber){}

    public void subscribeEvents(String eventName){}

    public void notifyCustomerProductUpdate(long productArticleNumber){}

    public void notifySellerProductStock(long productArticleNumber){}
}
