package com.challenge.coderbyte;

public enum ProductType {

    Digital(Long.MAX_VALUE, "download Link / Email "), Normal(100, "post");

    long stock;
    String deliveryType;

    ProductType(long stockLimit, String deliveryType) {
        this.stock = stockLimit;
        this.deliveryType = deliveryType;
    }

    public String chooseDeliveryType(ProductType productType){return null;}
}
