package com.challenge.coderbyte;

import java.util.HashMap;

public class ShoppingCart {

    private HashMap<String, Product> productHash;
    private double grossPrice;
    private double finalPrice;

    public ShoppingCart(User user){}

    public void addProduct(Product product, User user){}

    public HashMap<String, Product> getShoppingCart(){
        return null;
    };

    public int getSize(){
        return 0;
    };

    public void removeProduct(long articleNumber, User user){}

    public void removeAll(){}

    public double findNetPrice(User user){
        return 0;
    };

    public void displayTotalProducts(HashMap<String, Product> productHash){};
}
