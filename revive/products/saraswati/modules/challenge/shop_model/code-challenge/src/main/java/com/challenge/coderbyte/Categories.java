package com.challenge.coderbyte;

import java.util.List;

public class Categories {
    String name;
    List<Product> products;

    static class Node {
        List<Product> products;
        Node left, right;

        Node(List<Product> products){
            this.products = products;
            left = null;
            right = null;
        }
    }

    public void displayProducts(List<Product> products){}
    public void displayCategoryHierarchy(Categories category){}
}
