package com.slabstech.codechallenge.house.house_man;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.slabstech.codechallenge.house.house_man")
@EnableJpaRepositories("com.slabstech.codechallenge.house.house_man.persistence.repo")
@EntityScan("com.slabstech.revive.server.springboot.persistence.model")
@ServletComponentScan
public class HouseManApplication {

	public static void main(String[] args) {
		SpringApplication.run(HouseManApplication.class, args);
	}

}
