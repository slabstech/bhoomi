package com.slabstech.codechallenge.house.house_man;

import com.slabstech.codechallenge.house.house_man.persistence.repo.HomeRepository;
import com.slabstech.codechallenge.house.house_man.web.HomeController;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.Charset;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
class HouseManApplicationTests {

	@MockBean
	private HomeRepository homeRepository;

	@Autowired
	HomeController homeController;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void whenHomeControllerInjected_thenNotNull() throws Exception {
		assertThat(homeController).isNotNull();
	}

	@Test
	public void whenGetRequestToHomes_thenCorrectResponse() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/homes")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

	}

	@Test
	public void whenPostRequestToHomesAndValidHome_thenCorrectResponse() throws Exception {
		MediaType textPlainUtf8 = new MediaType(MediaType.TEXT_PLAIN, Charset.forName("UTF-8"));
		String home = "{\"name\": \"bob\", \"email\" : \"bob@domain.com\"}";
		mockMvc.perform(MockMvcRequestBuilders.post("/homes")
						.content(home)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(textPlainUtf8));
	}

	@Test
	public void whenPostRequestToHomesAndInValidHome_thenCorrectReponse() throws Exception {
		String home = "{\"name\": \"\", \"email\" : \"bob@domain.com\"}";
		mockMvc.perform(MockMvcRequestBuilders.post("/homes")
						.content(home)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Is.is("Name is mandatory")))
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
	}
}
