package com.slabstech.codechallenge.house.house_man.persistence.repo;

import com.slabstech.codechallenge.house.house_man.persistence.model.Home;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeRepository extends JpaRepository<Home, Long> {
}
