package com.slabstech.codechallenge.house.house_man.persistence.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.SequenceGenerator;

import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "home")
@EntityListeners(AuditingEntityListener.class)
public class Home {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "home_generator")
    @SequenceGenerator(name="home_generator", sequenceName = "home_id_seq", allocationSize = 1)
    @Column(name = "id")
    private long id;

    @NotBlank(message = "Address must not be blank")
    @Size(min = 4, max = 50)
    @Column(name = "address")
    String address;

    @NotBlank(message = "Photos must not be blank")
    @Size(min = 6, max = 255)
    @Column(name = "photos_url")
    String photos_url;

    @NotBlank(message = "Living Area Size must not be blank")
    @Column(name = "living_area")
    float living_area;


    @NotBlank(message = "Rent Price must not be blank")
    @Size(min = 250, max = 5000)
    @Column(name = "rent_price")
    float rent_price;

    @NotBlank(message = "Warm Price must not be blank")
    @Size(min = 25, max = 500)
    @Column(name = "warm_extra_price")
    String warmExtraPrice;

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getPhotos_url() {
        return photos_url;
    }

    public float getLiving_area() {
        return living_area;
    }

    public float getRent_price() {
        return rent_price;
    }

    public String getWarmExtraPrice() {
        return warmExtraPrice;
    }

}
