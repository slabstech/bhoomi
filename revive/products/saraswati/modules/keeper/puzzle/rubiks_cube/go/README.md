#### Rubiks Cube Solver

This is a simple implementation of a Rubik's Cube solver in Go. It uses a breadth-first search to find the shortest path to a solved cube. It is not optimized for speed, but it is optimized for readability.



* Cube - 6x3x3 - z, y, x

* Requirements
  * Should execute on a Raspberry Pi 3/4/Zero
  * Should be able to solve a 6x3x3 cube in under 10 seconds
  * Should use the least amount of memory possible
    * Constraint to be verified by running on Docker with a 128MB memory limit

* Expected Output
  * Level 1 - A list of moves to solve the cube
  * Level 2 - A list of moves to solve the cube in reverse
  * Level 3 - A list of moves to solve the cube in reverse, but with the middle layer solved first


* Each face is a 3x3 matrix with fixed center value
  * 0 - white - [0, 1, 1]
  * 1 - red - [1, 1, 1]
  * 2 - green - [2, 1, 1]
  * 3 - blue - [3, 1, 1]
  * 4 - orange - [4, 1, 1]
  * 5 - yellow - [5, 1, 1]


* Equation for movement
  * z = z % 6
    * Right - +1
    * Left - +3
    * Up - +4
    * Down - +5
  * y = y % 3
    * Right - 0
    * Left - 0
    * Up - +3
    * Down - +3
  * x = x % 3
    * Right - +1
    * Left - +2
    * Up - 0
    * Down - 0