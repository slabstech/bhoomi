# keeper
Puzzles to choose new collaborators. Name inspired from Netflix Culture Deck

* Easy - Tic-Tac-Toe
* Medium - 2048
* Hard - Rubik's Cube, Sudoku 5x5, 7x7, 10x10

#### Guidelines
* Choose any language of choice
* Upload solution repository link as PR. [Example PR](https://github.com/keeper) 
* Check the high score at [keeper](https://slabstech.com/keeper)

P.S - Top Score are updated every Saturday.


#### TODO
* Create GitHub to update score every Saturday
* Create Example PR for steps
