package com.test.game;

import java.util.ArrayList;
import java.util.List;

public class GameOfKnights {

	public static void main(String[] args) {

		List<Knight> gameKnights = new ArrayList<Knight>();

		Knight k1 = new Knight("k1");
		Knight k2 = new Knight("k2");
		Knight k3 = new Knight("k3");

		gameKnights.add(k1);
		gameKnights.add(k2);
		gameKnights.add(k3);

		// implement turn

		int max = 4, min = 1;

		int max_knights = 3;

		int start_position = 0;
		while (gameKnights.size() > 1) {

			Knight current = gameKnights.get(start_position);

			System.out.println("Attacker Knight Name " + current.getName());

			int randomHitPoint = (int) (Math.random() * (max - min + 1) + min);

			System.out.println("HitPoints for current Turn :" + randomHitPoint);

			int successorPosition = (start_position + 1) % max_knights;
			Knight successor = gameKnights.get(successorPosition);

			int successorPoints = successor.getHitPoints();

			if (successorPoints - randomHitPoint <= 0) {
				System.out.println("Knight " + successor.getName() + "  has died...");
				gameKnights.remove(successorPosition);

				max_knights--;
			} else {

				System.out.println("Knight " + current.getName() + " deals " + randomHitPoint + " damage to knight "
						+ successor.getName() + "");
				successor.setHitPoints(successorPoints - randomHitPoint);
				gameKnights.set(successorPosition, successor);
			}

			start_position = (start_position + 1) % max_knights;

			System.out.println("Attacker HitPoints - " + current.toString() + " - Successor " + successor.toString());

		}

		System.out.println(gameKnights.get(start_position) + " stands victorious!");

	}

}

class Knight {
	String name;
	int hitPoints = 5;

	public Knight(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
	}

	@Override
	public String toString() {
		return "Knight [name=" + name + ", hitPoints=" + hitPoints + "]";
	}

}
