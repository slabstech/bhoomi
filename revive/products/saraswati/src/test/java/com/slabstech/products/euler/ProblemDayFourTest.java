package com.slabstech.products.euler;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ProblemDayFourTest {

    @Test
    void solveTaskOne() {

        int []A = {-1,1,2,-2};

        ProblemDayFour pbFour = new ProblemDayFour();

        assertThat(pbFour.solveTaskOne(A)).isEqualTo(-2);
    }

    @Test
    void solveTaskTwo() {

        String A = "codility";
        String B = "abbabba";

        ProblemDayFour pbFour = new ProblemDayFour();

        assertThat(pbFour.solveTaskTwo(A)).isEqualTo(0);

        assertThat(pbFour.solveTaskTwo(B)).isEqualTo(4);


    }

    @Test
    void solveTaskThree() {


    ProblemDayFour pbFour = new ProblemDayFour();

    String S = "3x2x";
    String T = "8";
    assertThat(pbFour.solveTaskThree(S,T)).isEqualTo(false);

    S = "ba1";
    T = "bA1";
    assertThat(pbFour.solveTaskThree(S,T)).isEqualTo(false);


    S = "a10";
    T = "10a";
    assertThat(pbFour.solveTaskThree(S,T)).isEqualTo(true);

    S = "A2Le";
    T = "2pL1";
    assertThat(pbFour.solveTaskThree(S,T)).isEqualTo(true);
    }
}