package com.slabstech.products.euler;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ProblemDayOneTest {

    @Test
    void solutionBinaryGap() {

        ProblemDayOne pbOne = new ProblemDayOne();

        Map<Integer, Integer> testValues = new HashMap<>();
        testValues.put(9,2);
        testValues.put(529,4);
        testValues.put(20,1);
        testValues.put(15,0);
        testValues.put(32,0);
        testValues.put(1041,5);

        int val = pbOne.solutionBinaryGap(Integer.MAX_VALUE);
        System.out.println(val + " " + Integer.MAX_VALUE);

         val = pbOne.solutionBinaryGap(Integer.MIN_VALUE);
        System.out.println(val + " " + Integer.MIN_VALUE);
        for(Integer key:testValues.keySet()){
            assertThat(pbOne.solutionBinaryGap(key)).isEqualTo(testValues.get(key)) ;
        }

    }
    @Test
    void problemOne() {

        ProblemDayOne pbOne = new ProblemDayOne();

        assertThat(pbOne.problemOne(10)).isEqualTo(23);
    }

    @Test
    void missingElement() {

        ProblemDayOne pbOne = new ProblemDayOne();

        int array[] = {2,3,1,5};

        assertThat(pbOne.missingElement(array)).isEqualTo(4);
    }

    @Test
    void oddOccurrenceInArray() {

        ProblemDayOne pbOne = new ProblemDayOne();
        int []A = {1,4,2,3};
        assertThat(pbOne.oddOccurrenceInArray(A)).isEqualTo(1);
    }

    @Test
    void problemTwo() {

        ProblemDayOne pbOne = new ProblemDayOne();
        assertThat(pbOne.problemTwo(5)).isEqualTo(0);
    }
}