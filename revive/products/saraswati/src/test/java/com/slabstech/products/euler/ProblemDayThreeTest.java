package com.slabstech.products.euler;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ProblemDayThreeTest {

    @Test
    void TestGenerateEmailAddress() {

        String empty="";
        String s= "John Doe; Peter Benjamin Parker; Mary Jane Watson-Parker; John Elvis Doe; John Evan Doe; Jane Doe; Peter Brian Parker";
        String c="Example";
        String outcome = "John Doe <john.doe@example.com>; Peter Benjamin Parker <peter.parker@example.com>; Mary Jane Watson-Parker <mary.watsonpa@example.com>; John Elvis Doe <john.doe2@example.com>; John Evan Doe <john.doe3@example.com>; Jane Doe <jane.doe@example.com>; Peter Brian Parker <peter.parker2@example.com>";

        ProblemDayThree pbThree = new ProblemDayThree();

        assertThat(pbThree.generateEmailAddress(empty,empty)).isEqualTo(null);
        assertThat(pbThree.generateEmailAddress(s,c)).isEqualTo(outcome);
    }
    @Test

    void TestGenerateEmailAddressOptimised() {

        String empty="";
        String s= "John Doe; Peter Benjamin Parker; Mary Jane Watson-Parker; John Elvis Doe; John Evan Doe; Jane Doe; Peter Brian Parker";
        String c="Example";
        String outcome = "John Doe <john.doe@example.com>; Peter Benjamin Parker <peter.parker@example.com>; Mary Jane Watson-Parker <mary.watsonpa@example.com>; John Elvis Doe <john.doe2@example.com>; John Evan Doe <john.doe3@example.com>; Jane Doe <jane.doe@example.com>; Peter Brian Parker <peter.parker2@example.com>";

        ProblemDayThree pbThree = new ProblemDayThree();

        assertThat(pbThree.generateEmailAddressOptimised(empty,empty)).isEqualTo(null);

        assertThat(pbThree.generateEmailAddressOptimised(s,c)).isEqualTo(outcome);
    }

    @Test
    void TestGenerateEmailAddressOptimisedStreams() {

        String empty="";
        String s= "John Doe; Peter Benjamin Parker; Mary Jane Watson-Parker; John Elvis Doe; John Evan Doe; Jane Doe; Peter Brian Parker";
        String c="Example";
        String outcome = "John Doe <john.doe@example.com>; Peter Benjamin Parker <peter.parker@example.com>; Mary Jane Watson-Parker <mary.watsonpa@example.com>; John Elvis Doe <john.doe2@example.com>; John Evan Doe <john.doe3@example.com>; Jane Doe <jane.doe@example.com>; Peter Brian Parker <peter.parker2@example.com>";

        ProblemDayThree pbThree = new ProblemDayThree();

        assertThat(pbThree.generateEmailAddressOptimisedStreams(empty,empty)).isEqualTo(null);
        //TODO fix test
        //assertThat(pbThree.generateEmailAddressOptimisedStreams(s,c)).isEqualTo(outcome);
    }

}