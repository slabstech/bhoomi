package com.slabstech.products.euler;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ProblemDayTwoTest {

    @Test
    void TestFrogJump() {

        ProblemDayTwo pbTwo = new ProblemDayTwo();
        assertThat(pbTwo.frogJump(10,1000000000,30)).isEqualTo(33333333);
        assertThat(pbTwo.frogJump(10,70,30)).isEqualTo(2);
        assertThat(pbTwo.frogJump(10,5,30)).isEqualTo(0);
        assertThat(pbTwo.frogJump(10,85,30)).isEqualTo(3);
    }

    @Test
    void TestTapeEquilibrium() {
        ProblemDayTwo pbTwo = new ProblemDayTwo();
        int input[] = {3,1,2,4,3};
        assertThat(pbTwo.tapeEquilibrium(input)).isEqualTo(1);
    }

    // Check if array is permutation
    /*


For example, given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2

the function should return 1.

Given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3

the function should return 0.

     */

    /*
    Distinct: Compute number of distinct values in an array.


    N is an integer within the range [0..100,000]; each element of array A is an integer within the range [−1,000,000..1,000,000]. For example, given array A consisting of six elements such that:

 A[0] = 2    A[1] = 1    A[2] = 1
 A[3] = 2    A[4] = 3    A[5] = 1

the function should return 3, because there are 3 distinct values appearing in array A, namely 1, 2 and 3.

     */

    /*


    Triangle: Determine whether a triangle can be built from a given set of edges.

A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0 ≤ P < Q < R < N and:

A[P] + A[Q] > A[R],
A[Q] + A[R] > A[P],
A[R] + A[P] > A[Q].

For example, consider array A such that:

  A[0] = 10    A[1] = 2    A[2] = 5
  A[3] = 1     A[4] = 8    A[5] = 20

Triplet (0, 2, 4) is triangular.

     */

    /*


    MaxProductOfThree: Maximize A[P] * A[Q] * A[R] for any triplet (P, Q, R).

A non-empty zero-indexed array A consisting of N integers is given. The product of triplet (P, Q, R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).

For example, array A such that:

  A[0] = -3
  A[1] = 1
  A[2] = 2
  A[3] = -2
  A[4] = 5
  A[5] = 6

contains the following example triplets:

(0, 1, 2), product is −3 * 1 * 2 = −6
(1, 2, 4), product is 1 * 2 * 5 = 10
(2, 4, 5), product is 2 * 5 * 6 = 60

Your goal is to find the maximal product of any triplet.

     */

}