package com.slabstech.products.puzzles.rubikcube;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.assertj.core.api.Assertions.*;

class RubikCubeTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void TestCubeInitialization() {
        //arrange
        Random rand = new Random();
        int cubeFaces = 6;
        int faceSize = 9;
        Integer randomSide = rand.nextInt(cubeFaces);

        //act
        RubikCube rubikCube = new RubikCube();

        //assert
        assertThat(rubikCube.getSides().size()).isEqualTo(cubeFaces);
        assertThat(rubikCube.getSides().get(randomSide).size()).isEqualTo(faceSize);
    }

    @Disabled
    @Test
    void TestCubeCompleteSuccess(){
        //arrange

        int cubeFaces = 6;
        int faceSize = 9;

        //act
        RubikCube rubikCube = new RubikCube();

        //assert
        assertThat(rubikCube.isComplete()).isEqualTo(true);

    }
    @Test
    void TestCubeCompleteFailure(){
        //arrange

        int cubeFaces = 6;
        int faceSize = 9;

        //act
        RubikCube rubikCube = new RubikCube();

        //assert
        assertThat(rubikCube.isComplete()).isEqualTo(false);
    }

}