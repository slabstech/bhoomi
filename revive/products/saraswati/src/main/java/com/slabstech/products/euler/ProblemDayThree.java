package com.slabstech.products.euler;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class ProblemDayThree {

    public String generateEmailAddress(String S, String C) {
        String generatedAddress = "";
        // Check for empty list
        if(S.length() ==0 || C.length()==0)
            return null;

        List<String> input = Arrays.asList(S.split(";"));
        // create unique email
        Map<String, Integer> uniqueEmails = new HashMap<>();

        for(String name:input){
            String[] splitName = name.trim().split(" ");
            String email="";
            if(splitName.length==3){
                String fname = splitName[0];
                String lname = splitName[2].toLowerCase().replace("-","");
                email = fname.toLowerCase() + "." + lname.substring(0, Math.min(lname.length(),8));
            }else if (splitName.length==2){
                String fname = splitName[0];
                String lname = splitName[1].toLowerCase().replace("-","");
                email = fname.toLowerCase() + "." + lname.substring(0, Math.min(lname.length(),8));
            }else
                email = splitName[0].toLowerCase();

            if(uniqueEmails.containsKey(email))
            {
                int count = uniqueEmails.get(email);
                count ++;
                uniqueEmails.put(email, count);
                email = email + count;

            }else {
                uniqueEmails.put(email,1);
            }

            email = " <" + email + "@" + C.toLowerCase() + ".com>; ";
            generatedAddress = generatedAddress + name.trim() + email ;
            }
        //remove last semi colon
        generatedAddress = generatedAddress.substring(0, generatedAddress.length()-2);

        return generatedAddress;
    }

    public String generateEmailAddressOptimised(String S, String C) {

        StringBuilder generatedAddress = new StringBuilder();
        // Check for empty list
        if(S.length() ==0 || C.length()==0)
            return null;

        String input[] = S.split(";");

        final char dot='.';
        final char at='@';
        final char startBracket = '<';
        final char endBracket = '>';
        final String dotCom = ".com";
        final char separator = ';';
        final String hypenToRemove = "-";
        final char spaceSeparator = ' ';
        // create unique email

        C= C.toLowerCase();
        Map<String, Integer> uniqueEmails = new HashMap<>();

        for(String name:input){
            String[] splitName = name.trim().split(" ");
            StringBuilder email= new StringBuilder();
            if(splitName.length==3){
                String lname = splitName[2].toLowerCase().replace(hypenToRemove,"");
                lname =  lname.substring(0, Math.min(lname.length(),8));
                email.append(splitName[0].toLowerCase());
                email.append(dot);
                email.append(lname);
            }else if (splitName.length==2){
                String lname = splitName[1].toLowerCase().replace(hypenToRemove,"");
                lname =  lname.substring(0, Math.min(lname.length(),8));
                email.append(splitName[0].toLowerCase());
                email.append(dot);
                email.append(lname);
            }else
                email.append(splitName[0].toLowerCase());

            if(uniqueEmails.containsKey(email.toString()))
            {
                int count = uniqueEmails.get(email.toString());
                count ++;
                uniqueEmails.put(email.toString(), count);
                email.append(count);

            }else {
                uniqueEmails.put(email.toString(),1);
            }

            generatedAddress.append(name.trim()) ;
            generatedAddress.append(spaceSeparator);
            generatedAddress.append(startBracket);
            generatedAddress.append(email);
            generatedAddress.append(at);
            generatedAddress.append(C) ;
            generatedAddress.append(dotCom);
            generatedAddress.append(endBracket);
            generatedAddress.append(separator);
            generatedAddress.append(spaceSeparator);
        }

        String result = generatedAddress.toString();
        //remove last semi colon
        result = result.substring(0, result.length()-2);

        return result;
    }

    public String generateEmailAddressOptimisedStreams(String S, String C) {


        // Check for empty list
        if(S.length() ==0 || C.length()==0)
            return null;

        Stream<String> input = Stream.of(S.split(";"));

       // int noOfElements =(int) input.count();

        StringBuilder generatedAddress = new StringBuilder();
        final char dot='.';
        final char at='@';
        final char startBracket = '<';
        final char endBracket = '>';
        final String dotCom = ".com";
        final char separator = ';';
        final String hypenToRemove = "-";
        final char spaceSeparator = ' ';
        // create unique email

        final String company= C.toLowerCase();
        Map<String, Integer> uniqueEmails = new HashMap<>();


        Map<String, String >value = Arrays.stream(S.split(";"))
                .map( name -> name.trim().split(" "))
                        .collect(toMap( name-> String.join(" ",name)  , name -> {
                            StringBuilder email= new StringBuilder();
                            String lname = null;
                            switch (name.length){
                                case 2:
                                    lname = name[1].toLowerCase().replace(hypenToRemove,"");
                                    break;
                                case 3:
                                    lname = name[2].toLowerCase().replace(hypenToRemove,"");
                                    break;
                                default: break;
                            }
                            lname =  lname.substring(0, Math.min(lname.length(),8));
                            email.append(name[0].toLowerCase());
                            email.append(dot);
                            email.append(lname);
                            return email.toString();
                        } ));


        value.forEach( (k,v) -> {
            if(uniqueEmails.containsKey(v)){
                int count = uniqueEmails.get(v);
                count ++;
                uniqueEmails.put(v, count);
                v = v+count;
                value.put(k,v);
            }else
                uniqueEmails.put(v,0);
        });

        System.out.println(value.size());

        value.forEach((k,v) -> {
            generatedAddress.append(k.trim()) ;
            generatedAddress.append(spaceSeparator);
            generatedAddress.append(startBracket);
            generatedAddress.append(v);
            generatedAddress.append(at);
            generatedAddress.append(company) ;
            generatedAddress.append(dotCom);
            generatedAddress.append(endBracket);
            generatedAddress.append(separator);
            generatedAddress.append(spaceSeparator);
        });

        String result = generatedAddress.toString();
        //remove last semi colon
        result = result.substring(0, result.length()-2);

        return result;
    }
}