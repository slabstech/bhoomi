package com.slabstech.products.puzzles.rubikcube;

public class Main {
    public static void main(String[] args) {
        System.out.println("Rubik Cube Solver !!");

        Solver solver = new Solver();

        RubikCube cube = new RubikCube();

        Moves moves = solver.solve(cube);

    }
}