package com.slabstech.products.euler;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ProblemDayFour {

    public int solveTaskOne(int []A){
        int ans = 0;
        for (int i = 0; i < A.length; i++) {
            if (ans > A[i]) {
                ans = A[i];
            }
        }
        return ans;

    }

    // Write a function that returns the length of longest string, that is both proper prefix and suffix of input string
    public int solveTaskTwo(String A){

        Set<String> prefix = new HashSet<>();
        StringBuilder prefixBuilder = new StringBuilder(A.length());
        for(int i=0; i< A.length()-1;i++){
            prefixBuilder.append(A.charAt(i));
            prefix.add(prefixBuilder.toString());
        }

        //prefix.remove(A);

        StringBuilder suffixBuilder = new StringBuilder();

        int maxLength =0;
        for(int i=A.length()-1; i> 0;i--){
            suffixBuilder.insert(0,A.charAt(i));
            if(prefix.contains(suffixBuilder.toString()))
            {
                maxLength = Math.max(maxLength, suffixBuilder.length());
            }
        }


        return maxLength;
    }
    public boolean solveTaskThree(String S, String T){

        // if strings are unequal
        if(S.length() != T.length())
            return false;

        // check for nonmatching case
        String sOnlyChars[] = S.replaceAll("[0-9]*", "").split("");

        Arrays.sort(sOnlyChars);


        String TOnlyChars[] = T.replaceAll("[0-9]*", "").split("");

        Arrays.sort(TOnlyChars);

        for(int i=0;i< sOnlyChars.length;i++){
            for(int j=0;j<TOnlyChars.length;j++){
                if (sOnlyChars[i].equalsIgnoreCase(TOnlyChars[j])) {
                    if (Character.compare(sOnlyChars[i].charAt(0),TOnlyChars[j].charAt(0)) !=0) {
                        return false;
                    }
                }
            }
        }

        // do regular express match
        StringBuilder sMatch = new StringBuilder();
        StringBuilder tMatch = new StringBuilder();

        for(int i=0;i<S.length();i++)
        {
            if(Character.isDigit(S.charAt(i))){
                Integer count = Integer.parseInt(String.valueOf(S.charAt(i)));
                for(int k=0;k< count;k++)
                sMatch.append('*');
            }
            else
                sMatch.append(S.charAt(i));
        }

        for(int i=0;i<T.length();i++)
        {
            if(Character.isDigit(T.charAt(i))){
                Integer count = Integer.parseInt(String.valueOf(T.charAt(i)));
                for(int k=0;k< count;k++)
                    tMatch.append('*');
            }
            else
                tMatch.append(T.charAt(i));

        }

        String sGen = sMatch.toString();
        String tGen = tMatch.toString();

        for(int i=0;i< sGen.length();i++){
            if(sGen.charAt(i) == '*')
                continue;
            if(tGen.charAt(i) == '*')
                continue;
            if(Character.compare(sGen.charAt(i),tGen.charAt(i)) ==0)
                continue;

            return false;

        }

        return true;
    }
}
