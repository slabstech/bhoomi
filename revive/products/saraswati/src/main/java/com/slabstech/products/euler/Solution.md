package com.slabstech.products.euler;
import java.util.*;


        import org.springframework.data.jpa.repository.*;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.MediaType;
        import org.springframework.transaction.annotation.*;
        import org.springframework.web.bind.annotation.*;
        import org.springframework.beans.factory.annotation.Autowired;

        import javax.persistence.*;
        import java.util.logging.Logger;
        import javax.validation.ConstraintViolation;
        import javax.validation.ConstraintViolationException;
        import javax.validation.Valid;
        import javax.validation.Validator;

        import org.springframework.data.jpa.domain.support.AuditingEntityListener;

        import javax.persistence.*;
        import javax.validation.constraints.NotBlank;
        import javax.validation.constraints.Size;

        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.validation.FieldError;
        import org.springframework.web.bind.MethodArgumentNotValidException;
        import org.springframework.web.bind.annotation.*;

        import java.util.*;

        // you can also use imports, for example:
        import java.util.*;
// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

public class Solution {


@Entity
@Table(name = "task")
@EntityListeners(AuditingEntityListener.class)
class Task {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @NotBlank(message = "Task description is required")
    @Size(min = 4, max = 200)
    @Column(name = "description")
    String description;


    @Column(name = "priority")
    private long priority = 1;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

}


@RestController
@RequestMapping("/tasks")
class TaskController {
    private static Logger log = Logger.getLogger("Solution");
    // log.info("You can use 'log' for debug messages");

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private Validator validator;
}
}

    /**
     * Get all Tasks list.
     *
     * @return the list
     */

    /*
    @GetMapping				   // GET Method for reading operation
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }


     */
    /**
     * Update task response entity.
     *
     * @param taskId the task id
     * @param taskDetails the task details
     * @return the response entity
     * @throws Exception
     */
/*
    @PutMapping("/{id}")    // PUT Method for Update operation
    public ResponseEntity<Task> updateTask(
            @PathVariable(value = "id") Long taskId, @Valid @RequestBody Task taskDetails)
            throws Exception {

        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException("Cannot find task with given id"));


        // Set the values for task here to update
        task.setDescription(taskDetails.getDescription());
        task.setPriority(taskDetails.getPriority());


        Set<ConstraintViolation<Task>> violations = validator.validate(task);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<Task> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }

            throw new ConstraintViolationException(sb.toString(), violations);
        }


        final Task updatedTask = taskRepository.save(task);
        return ResponseEntity.ok(updatedTask);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


}

class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super();
    }

    public TaskNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TaskNotFoundException(final String message) {
        super(message);
    }

    public TaskNotFoundException(final Throwable cause) {
        super(cause);
    }
}

interface TaskRepository extends JpaRepository<Task, Long> {

}
*/