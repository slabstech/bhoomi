package com.slabstech.products.puzzles.rubikcube;

import java.awt.*;
import java.awt.geom.Point2D;

public class Moves {

    /*


Equation for movement

    z = z % 6
        Right - +1
        Left - +3
        Up - +4
        Down - +5
    y = y % 3
        Right - 0
        Left - 0
        Up - +3
        Down - +3
    x = x % 3
        Right - +1
        Left - +2
        Up - 0
        Down - 0


     */
    public Point[] moveUp(Point2D[] value){

        Point2D current = value[0];
        double x = (current.getX() + 0)%3 ;
        double y = (current.getY() + 3)%3;
        current.setLocation(x,y);
        return null;
    }
    public Point2D[] moveDown(Point2D[] value){return null;}
    public Point2D[] moveLeft(Point2D[] value){return null;}
    public Point2D[] moveRight(Point2D[] value){return null;}

}
