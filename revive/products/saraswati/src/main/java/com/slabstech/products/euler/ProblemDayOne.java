package com.slabstech.products.euler;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ProblemDayOne {

    //
    public int solutionBinaryGap(int N){

        String value = Integer.toBinaryString(N);

        int count=0;

        for(int i=0; i< value.length()-1;i++)
        {
            for(int j=i+1; j< value.length();j++){
                if(value.charAt(i)=='1' && value.charAt(j)=='1'){
                    count = Math.max(count,j-i -1);
                    break;
                }
            }
        }
        return count;
    }

    public int oddOccurrenceInArray(int[] A){
        Set<Integer> pair= new HashSet<>();
        for(int i=0;i< A.length;i++){
            if(pair.contains(A[i])){
                pair.remove(A[i]);

            }else
                pair.add(A[i]);

        }

        return (Integer)pair.toArray()[0];
    }

    public int missingElement(int[] A){
        List<Integer> elements = Arrays.stream(A).boxed().toList();

        List<Integer> sorted = elements.stream().sorted().collect(Collectors.toList());
        //elements.sort((p1, p2) -> p1.compareTo(p2));
        //Collections.sort(elements);
        for(int i=0; i< sorted.size()-1;i++ ){
            if(sorted.get(i)  != (sorted.get(i+1) -1 ))
                return sorted.get(i) +1;
        }
        return 0;
    }

    // Find the sum of all the multiples of 3 or 5 below 1000.
    // https://projecteuler.net/problem=1
    public int problemOne(int N){
        AtomicInteger sum= new AtomicInteger();

        Set<Integer> values = new HashSet<>();
        for(int i=0;i<N; i++){
            if(i%3==0 ||i%5==0 )
                values.add(i);      // sum = sum + i ;
        }
        values.forEach( n -> sum.set(sum.get() + n));

        return sum.get();
    }

    //https://projecteuler.net/problem=2
    //By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
    public int problemTwo(int N){

        List<Integer> fibonacciNumbers = new ArrayList<>();
        fibonacciNumbers.add(0);
        fibonacciNumbers.add(1);

        return 0;
    }

    // * leet code : https://leetcode.com/problems/merge-sorted-array/description/
    // Merge nums1 and nums2 into a single array sorted in non-decreasing order.
    // Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
    //Output: [1,2,2,3,5,6]

    // Fizzbuzz
    // https://leetcode.com/problems/fizz-buzz/description/
    //Input: n = 15
    //Output: ["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz"


    // Copy List with Random Poiunter
    // https://leetcode.com/problems/copy-list-with-random-pointer/
    /*
// Definition for a Node.
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
*/
    //Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
    //Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]

    // https://leetcode.com/problems/serialize-and-deserialize-binary-tree/description/
    /**
     * Definition for a binary tree node.
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    /*
    public class Codec {

        // Encodes a tree to a single string.
        public String serialize(TreeNode root) {

        }

        // Decodes your encoded data to tree.
        public TreeNode deserialize(String data) {

        }
    }

     */

// Your Codec object will be instantiated and called as such:
// Codec ser = new Codec();
// Codec deser = new Codec();
// TreeNode ans = deser.deserialize(ser.serialize(root));

    // No. of Islands
    //https://leetcode.com/problems/number-of-islands/description/

    /*
    Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1

Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3
Constraints:

    m == grid.length
    n == grid[i].length
    1 <= m, n <= 300
    grid[i][j] is '0' or '1'.

    class Solution {
    public int numIslands(char[][] grid) {

    }
}

     */
   // 114. Flatten Binary Tree to Linked List
    // https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
/*
    Input: root = [1,2,5,3,4,null,6]
    Output: [1,null,2,null,3,null,4,null,5,null,6]
Input: root = [0]
Output: [0]


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
        /*
    class Solution {
        public:
        void flatten(TreeNode* root) {

        }
    };
 */

// Valid palindrome
    // https://leetcode.com/problems/valid-palindrome/
    /*

    Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.

Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.
     */

    /*
    public boolean isPalindrome(String s) {

    }
    */

 // Longest Palindrone subsequence
 // https://leetcode.com/problems/longest-palindromic-substring/

}
