package com.slabstech.products.euler;

public class ProblemDayTwo {

    public int frogJump(int X, int Y, int D){
        int count =0;
        if(D > 0){
            count = (Y-X)/D;
            int rem = (Y-X)%D;
            if(rem > 0)
                count++;
        }

        /*

        while(Y > X){
            X = X +D ;
            count++;
        }
        */
        return count;
    }

    public int tapeEquilibrium(int[]A){

        int min=Integer.MAX_VALUE;

        for (int i=1; i< A.length;i++){
            int sumA=0, sumB=0;
            int j=0;
            while(j<=i){
                sumA+=A[j];
                j++;
            }
            while (j< A.length){
                sumB+=A[j];
                j++;
            }
            min = Math.min(min,Math.abs(sumA-sumB));
        }

        // reverse the logic - Sum all vslue and reduce the arrays to reduce calcualtion .
        return min;
    }
}
