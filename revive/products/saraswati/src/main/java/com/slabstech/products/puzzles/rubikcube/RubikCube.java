package com.slabstech.products.puzzles.rubikcube;

import java.util.*;

public class RubikCube {

    private List<List<Integer>> sides;
    private int cubeFaces = 6;
    private int faceSize = 9;

    private int centerIndex = 4;

    RubikCube() {
        sides = new ArrayList<>();
        initCube();
    }

    public List<List<Integer>> getSides() {
        return sides;
    }

    public void initCube() {

        // 6 faces/type . 9 values
        Random rand = new Random();
        Map<Integer, Integer> countMap = new HashMap<>();
        for (int i = 0; i < cubeFaces; i++) {
            List<Integer> face = new ArrayList<>();

            for (int j = 0; j < faceSize; ) {
                Integer value = rand.nextInt(cubeFaces);
                if (countMap.containsKey(value)) {
                    Integer count = countMap.get(value);
                    if (count < faceSize) {
                        count++;
                        countMap.put(value, count);
                        face.add(value);
                        j++;
                    }
                } else {
                    countMap.put(value, 0);
                    face.add(value);
                    j++;
                }
            }
            sides.add(face.stream().toList());
        }
    }

    public boolean isComplete() {

        for (int i = 0; i < cubeFaces; i++) {
            List<Integer> face = sides.get(i);
            Integer centerFace = face.get(centerIndex);
            for (int j = 0; j < faceSize; j++) {
                if(centerFace != face.get(j))
                    return false;
            }
        }
        return true;
    }
}
