rm -rf .book
mkdir .book

cd docs/_posts/wiki
for f in *.md; do grip --export $f --no-inline; done

mv *.html ../../../.book
cd ../../../
cd .book

cat readme.html 2022-06-26-build.html 2022-06-26-use_cases.html 2022-06-26-sprint.html 2022-06-26-faqs.html 2022-06-26-ci_cd.html 2022-06-26-persistence.html 2022-06-26-test.html 2022-06-26-ui-design.html 2022-06-26-release.html 2022-06-26-sprint-logs.html  > revive.html


wkhtmltopdf revive.html revive.pdf
