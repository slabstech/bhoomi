<!-- Keep a Changelog guide -> https://keepachangelog.com -->

# Revive Changelog

<!-- 
## [0.0.1]
-->

## [Unreleased]
### Added
- Initial project scaffold
- GitHub Actions to automate testing and deployment
- Kotlin support