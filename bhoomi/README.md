# ಭೂಮಿ / Bhoomi

#### Habitat Monitoring System

Assistance system to help Martian explorers in Science Missions

* NASA Space Apps [2022 Global Nominee](https://2022.spaceappschallenge.org/locations/magdeburg/teams)

[Visit Website - https://mangala.earth](https://mangala.earth)


Measure  Build  Intervene

### Sponsor's Required- 360 €
* Phase 1 - AstroPi - for [Nasa Space Apps Challenge 2022 Global Nominee](https://mangala.earth/sponsors/spaceapps-2022/)
  * [Amazon Wishlist](https://www.amazon.de/hz/wishlist/ls/3GMSOW67OIS02) - 
    * Solar Panel for RaspberryPi 
    * Solar PowerBank
    * Pi Cluster Case
    * Smart Car Arduino Kit
    * [Battery and Charger for DJI Tell Drone](https://www.amazon.de/dp/B08HH28RJ3/) : 60 € 

### Ingenuity
* Mars Ingenuity - https://github.com/readme/featured/nasa-ingenuity-helicopter
* fprime - 
  * flight software nasa - https://github.com/nasa/fprime
  * https://nasa.github.io/fprime/INSTALL.html
* Mars Roto craft - https://www.asindia.org/mars-rotocraft

### Powered by
* S Labs Solutions, India. [link](https://slabstech.com)

<!-- Embed Generator https://www.labnol.org/embed/google/drive/ 
Manifest - https://www.mozilla.org/en-US/about/manifesto/
>